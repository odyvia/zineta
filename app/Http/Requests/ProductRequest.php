<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'price'=>'required|numeric|min:1',
            'qty'=>'required|numeric|min:1',
            'discount'=>'numeric|max:100|min:0',
            'images'=>'array|min:1',
            'images.*'=>'required|image',
            'description'=>'required',
            'category'=>'required|array|min:1'
        ];
    }

    public function messages(){
        return [
            'name.required'=>'Nama Produk tidak boleh kosong',
            'price.required'=>'Harga Produk tidak boleh kosong',
            'price.numeric'=>'Harga Produk harus angka',
            'price.min'=>'Harga Produk Minimal 1',
            'qty.required'=>'Quantity tidak bolleh kosong',
            'qty.numeric'=>'Quantity Produk harus angka',
            'qty.min'=>'Quantity Produk Minimal 1',
            'discount.numeric'=>'Diskon Produk harus angka',
            'discount.min'=>'Diskon Produk Minimal 0',
            'discount.max'=>'Diskon Produk Maksimal 100',
            'images.array'=>'Gambar produk tidak boleh kosong',
            'images.*.required'=>'Gambar Produk tidak boleh kosong',
            'description.required'=>'Deskripsi produk tidak boleh kosong',
            'category.array'=>'Kategory produk tidak boleh kosong',
            'category.required'=>'Kategory produk tidak boleh kosong',
        ];
    }

    public function prepareForValidation(){
        $this->merge([
            'category'=>json_decode($this->category),
            'images.*'=>is_string($this->images) ? json_decode($this->images) : $this->images
        ]);
    }
}
