<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Product;

class TransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        return [
            'qty'=>'required|numeric|min:1|max:'.$this->getCurrentQty($this->id),
            // 'size'=>'required'
        ];
    }

    public function messages(){
        return [
            'qty.required'=>'Jumlah beli harus di isi',
            'qty.max'=>'Jumlah beli melebihi stok',
            'qty.min'=>'Jumlah beli min 1',
            'qty.numeric'=>'Jumlah beli harus angka',
            // 'size.required'=>'Ukuran mohon di isi'
        ];
    }

    public function getCurrentQty($id){
        // $product = Product::with('productImage','size')->findOrFail($id);
        // $stock = $product->size()->wherePivot('size_id','=',$this->size)->pluck('qty');
        $product = Product::findOrFail($id);
        $stock= $product->qty;
        // return count($stock) > 0 ? $stock[0] : '';
        return $stock;
    }
}
