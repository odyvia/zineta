<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except('getCategories');    
    }

    public function getCategories(){
        $categories = Category::all();
        return $categories;
    }

    public function index(){
        $categories=Category::all();
        return view('backend.store.categories.index')
        ->with('title','List kategori')
        ->with('categories',$categories);
    }
    
    public function create(){
        return view('backend.store.categories.create')
        ->withTitle('Tambah Kategori');
    }

    public function store(Request $request){
        $this->validate($request,[
            'name'=>'required|unique:categories'
        ],[
            'name.required'=>'Nama Kategori wajib di isi',
            'name.unique'=>'Nama kategori sudah ada'
        ]);

        $model = new Category;
        $model->name = $request->input('name');
        $model->save();

        return redirect()->route('category.index')->with('status','success')->with('message','Data berhasil di simpan');
    }

    public function edit($id){
        $category = Category::findOrFail($id);
        return view('backend.store.categories.edit')
        ->withCategory($category)
        ->withTitle('Edit Kategori '.$category->name);
    }

    public function update(Request $request,$id){
        $this->validate($request,[
            'name'=>'required|unique:categories,id,'.$id
        ],[
            'name.required'=>'Nama Kategori wajib di isi',
            'name.unique'=>'Nama kategori sudah ada'
        ]);

        $model = Category::findOrFail($id);
        $model->name = $request->input('name');
        $model->save();

        return redirect()->route('category.index')->with('status','success')->with('message','Data berhasil di simpan');
    }

    public function destroy($id){
        $model = Category::findOrFail($id);
        $model->delete();

        return redirect()->route('category.index')->with('status','success')->with('message','Data berhasil di Hapus');
    }
    
}
