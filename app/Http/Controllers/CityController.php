<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\RajaOngkirHelper;

class CityController extends Controller
{
    public function getCities(Request $request){
        return RajaOngkirHelper::getData('city?province='.$request->input('id'));
    }
}
