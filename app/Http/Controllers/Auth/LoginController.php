<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Illuminate\Auth\Events\Registered;
use DB;
use App\Events\Auth\UserActivationEmail;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    protected function redirectTo()
    {
        if (auth()->user()->hasRole('Administrator')) {
            return '/transaction';
        }
        return '/';
    }

    
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        $authUser = $this->findOrCreateUser($user, $provider);
        
        
        if($authUser->email_verified_at == null){
            event(new UserActivationEmail($authUser));
            $this->guard()->logout();
            return redirect('/login')
                ->with('status','success')
                ->with('message','Registered. Please check your email to activate your account.');
        }else{
            Auth::login($authUser);         
            return redirect('/');
        }
    }

    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::whereHas('authProvider',function($query) use($user){
            $query->where('provider_id',$user->id);
        })->first();
        if ($authUser) {
            return $authUser;
        }
        else{
                $data = User::create([
                    'name'     => $user->name,
                    'email'    => !empty($user->email)? $user->email : '' ,
                    'password' => bcrypt('secret'),
                    'provider' => $provider,
                    'provider_id' => $user->id,
                    'image'=>$user->avatar,
                    'activation_token'=>Str::random(255)
                ]);
                $data->authProvider()->create(['provider_id'=>$user->id,'provider'=>$provider]);
                return $data;
        }
        
    }
}
