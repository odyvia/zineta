<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Lookbook;
use Image;
use Storage;
use Mail;
use ImageHelper;

class LookbookController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth'])->except('show');
    }


    public function sendEmail(Request $request)
    {
    $userid = "9238427772";
    $accessToken = "9238427772.1677ed0.a84f433d1f644b40914757133b0a6b27";
// Get our data


// Pull and parse data.
    
// foreach ($result->data as $post){
// 	if ($i < $limit ){
//         $i ++;
//     }
// } 
}

    public function index(){
        $lookbooks = Lookbook::all();
        return view('backend.lookbooks.index')
        ->with('lookbooks',$lookbooks);
    }

    public function create(){
        return view('backend.lookbooks.create');
    }

    public function show($id){
        $lookbook = Lookbook::with('detailLookbook')->find($id);
        return view('front_end.detail_lookbook')->with('lookbook',$lookbook);
    }
    
    public function edit($id){
        $lookbook = Lookbook::with('detailLookbook')->find($id);
        return view('backend.lookbooks.edit')
        ->with('lookbook',$lookbook);
    }

    public function update(Request $request,$id){
        $model = Lookbook::with('detailLookbook')->find($id);


        $model->caption = $request->input('caption');
        $main_image = $request->file('mainImage');
        $main_file_name = $main_image->getClientOriginalName();
        $main_image_path = $main_image->storeAs('public',$main_file_name);
        $model->image = $main_file_name;
        $model->save();

        $lookbook_images = $request->file('images');

        foreach ($lookbook_images as $image) {
            $detail_image = $image->getClientOriginalName();
            $detail_image_path = $image->storeAs('public',$detail_image);
            $model->detailLookbook()->create([
                'image'=>$detail_image
            ]);
        };
    }

    public function destroy($id){
        $model = Lookbook::find($id);
        $model->delete();

        return redirect('lookbook');
    }

    public function store(Request $request){
        $model = new Lookbook;
        $model->caption = $request->input('caption');
        $main_image = $request->file('mainImage');
        $main_file_name = ImageHelper::setName($main_image->getClientOriginalName());
        $main_image_path = $main_image->storeAs('public',$main_file_name);
        $model->image = $main_file_name;
        $model->save();

        $lookbook_images = $request->file('images');

        foreach ($lookbook_images as $image) {
            $detail_image = $image->getClientOriginalName();
            $detail_image_path = $image->storeAs('public',$detail_image);
            $model->detailLookbook()->create([
                'image'=>$detail_image
            ]);
        };

    }

    
}
