<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Site;
use ImageHelper;
use Storage,DB;
use App\Models\Product;

class SiteController extends Controller
{
    public function index(){
        $site=Site::with('carouselSite')->first();
        return '<img src="'.Storage::url($site->background_image).'" />';
    }

    public function create(){
        return view('backend.site.create')->withTitle('Site');
    }

    public function store(Request $request){
        $site=new Site;
        $site->site_name = $request->input('site_name');
        $site->phone = $request->input('phone');
        $site->address = $request->input('address');
        $bg_image = $request->file('background_image');
        

        $bg_image_name = ImageHelper::setName($bg_image->getClientOriginalName());
        $bg_image_name_path = $bg_image->storeAs('public',$bg_image_name);
           
        $site->background_image = $bg_image_name;
        $site->save();
    }

    public function edit(){
        $site=Site::first();
        return view('backend.site.edit')
        ->withSite($site)
        ->withTitle('Edit Site');
    }

    public function update(Request $request){
       DB::transaction(function() use($request){
        $site=Site::first();
        $site->site_name = $request->input('site_name');
        $site->phone = $request->input('phone');
        $site->address = $request->input('address');
        $bg_image = $request->file('background_image');
        if($bg_image!=null){
            $bg_image_name = ImageHelper::setName($bg_image->getClientOriginalName());
            Storage::disk('local')->delete('public/'.$site->background_image);
            $bg_image_name_path = $bg_image->storeAs('public',$bg_image_name);
            $site->background_image = $bg_image_name;
        }
           
        
        
        // DB::table('carousel_sites')->where('site_id',$site->id)->delete();

        foreach ($request->file('files') as $image) {
            $image_name = ImageHelper::setName($image->getClientOriginalName());
            $image_name_path = $image->storeAs('public',$image_name);
            $site->carouselSite()->create([
                'image'=>$image_name
            ]);
        };

        $site->save();
       });
    }

    public function destroy(){

    }

    public function dashboard(){
        $this->authorize('view.dashboard');
        $userTotal = \App\User::count();
        $lowestProduct = Product::lowestStock();
        return view('backend.site.dashboard')
        ->withTitle('Dashboard')
        ->withLowestProduct($lowestProduct)
        ->withUserTotal($userTotal);
    }
}
