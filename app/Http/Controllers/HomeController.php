<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware(['auth']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $userid = "9238427772";
        // $accessToken = "9238427772.1677ed0.a84f433d1f644b40914757133b0a6b27";
        // $url="https://api.instagram.com/v1/users/{$userid}/media/recent/?access_token={$accessToken}";
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $url);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        // $result = curl_exec($ch);
        // curl_close($ch);
        // $items = json_decode($result);
        // dd($items);
        return view('welcome');
        // ->with('items',$items);
    }
}
