<?php

namespace App\Http\Controllers;

use App\Models\Size;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    public function getSizes(){
        $sizes = Size::all();
        return $sizes;
    }

    public function getSizeName($id){
        $size=Size::findOrFail($id);
        return $size->name;
    }
}
