<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Cart,Auth;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Helpers\RajaOngkirHelper;
use App\Http\Requests\TransactionRequest;
use AutoNumberHelper;
use App\User;
use PDF,Validator;
use App\DataTables\TransactionDataTable;


class TransactionController extends Controller
{
    public function __construct(){
        $this->middleware(['auth','verified'])->except('setExpiredTransaction','notificationHandler');
        // midtrans config

        // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = 'SB-Mid-server-QQ9S6bAiJk3UOtD1xY_pp1_k';
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = false;
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;
    }

    public function updateResi(Request $request,$id){
        $this->authorize('resi.update');
        $transaction = Transaction::findOrFail($id);
        $transaction->no_resi = $request->input('no_resi');
        $transaction->save();
    }

    public function setExpiredTransaction(){
        $model = new Transaction;
        $transactions = Transaction::with('transactionDetail')->get();
        foreach ($transactions as $transaction) {
            $transaction->checkExpiredTransaction();
        }
    }

    public function cancelPayment($id){
        $transaction=Transaction::findOrFail($id);
        if(Auth::user()->id == $transaction->user_id){
            $transaction->setExpired();
        }
    }

    public function index(TransactionDataTable $dataTable){
        $this->authorize('transaction.list');
        $transactions = Transaction::all();
        $title='LIst Transaksi';
        // return view('backend.store.transactions.index')
        // ->withTitle('List Transaction')
        // ->withTransactions($transactions);

        return $dataTable->render('backend.store.transactions.index',compact('title'));
    }

    public function myTransaction(){ // fungsi untuk melihat list transaksi berdasrkan login user
        $transactions = Transaction::where('user_id',Auth::user()->id)->get();
        return view('front_end.store.transactions.index')
        ->withTitle('List Transaction')
        ->withTransactions($transactions);
    }
    
    public function addToCart(TransactionRequest $request,$productId){
        if(session('id') == null){
            session(['id' => Str::uuid()]);
        }
        $size = new \App\Models\Size;
        $product = Product::with('productImage')->findOrFail($productId);
        $image=$product->productImage->first();
        Cart::add($product->id,$product->name,$request->input('qty'),($product->price - ($product->price * $product->discount / 100)),1,['product_id'=>$product->id,'discount'=>$product->discount,'image'=>$image->name]);
        return redirect()->back()->with('success', 'Produk berhasil ditambahkan ke keranjang!');
    }

    public function payment($id){
        $transaction = Transaction::with(['transactionDetail','transactionPayment','user'])->find($id);
        $snapToken = null;
        $this->authorize('payment',$transaction);
        if(Auth::user()->can('resi.update')){
            $transaction->setHasViewed();
        }

        if($transaction->snap_token == null){
            $carts = Cart::content();
            $transaction_details = array(
                'order_id' =>$transaction->id,
                'gross_amount' => (int)(Cart::subtotal()), // no decimal allowed for creditcard
            );

            $shipping_address = array(
                'first_name'   => $transaction->name,
                'last_name'    => '',
                'address'      => $transaction->address,
                'city'         => $transaction->destination_details['city_name'],
                'postal_code'  => $transaction->destination_details['postal_code'],
                'phone'        => $transaction->phone
            );

            // Optional
            $customer_details = array(
                'first_name'    => $transaction->user->name,
                'last_name'     => '',
                'email'         => $transaction->user->email,
                'phone'         => $transaction->phone,
                // 'billing_address'  => $billing_address,
                'shipping_address' => $shipping_address
            );
            $tmp_ongkir = [
                'id' => rand(),
                'price' => $transaction->ongkir,
                'quantity' => 1,
                'name' => $transaction->courier 
            ];
            $item_details = [];
            foreach ($transaction->transactionDetail as $cart) { 
                array_push($item_details,[
                    'id' => $cart->id,
                    'price' => $cart->price,
                    'quantity' => $cart->qty,
                    'name' => $cart->name,
                ]);
            }
            array_push($item_details,$tmp_ongkir);
            // Fill transaction details
            $transaction_snap = array(
                'transaction_details' => $transaction_details,
                'customer_details' => $customer_details,
                'item_details' => $item_details,
            );
            $snapToken = \Snap::getSnapToken($transaction_snap);
            $transaction->snap_token = $snapToken;
            $transaction->save();
        }
        return view('front_end.store.payment')
        ->with('snap_token',$snapToken != null ? $snapToken : null)
        ->withTransaction($transaction);
    }

    public function shipment(){
        if(Cart::count() == 0){
            return redirect('cart');
        }
        return view('front_end.store.shipment');
    }

    public function checkout(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'phone'=>'required|numeric',
            'ongkir'=>'required',
            'alamat'=>'required'
        ],[
            'ongkir.required'=>'Mohon Pilih Ekspedisi',
            'name.required'=>'Nama Mohon di isi',
            'phone.required'=>'No Telepon mohon di isi',
            'phone.numeric'=>'No telepon tidak boleh karakter',
            'alamat.required'=>'Alamat lengkap mohon di isi'
        ]);
        $id = Str::uuid();

       \DB::transaction(function() use($request,$id){
            $subtotal = (int)(Cart::subtotal());
            $tmp_ongkir = $request->input('ongkir');
            $ongkir = $tmp_ongkir['cost'][0]['value'];
            $total = $subtotal;
            $model = new Transaction;
            $model->id = $id;
            $model->invoice = AutoNumberHelper::generateInvoiceNumber();
            $model->total = $total;
            $model->ongkir = $ongkir;
            $model->origin_details = $request->input('origin_details');
            $model->destination_details = $request->input('destination_details');
            $model->name = $request->input('name');
            $model->address = $request->input('alamat');
            $model->phone = $request->input('phone');
            $model->courier = $request->input('courier_id').' '.$tmp_ongkir['service'];
            $model->user_id = Auth::user()->id;
            $model->save();

            $carts =Cart::content();
            $item_details = [];
            if($model){
                foreach ($carts as $cart) {
                    $product= Product::findOrFail($cart->options->product_id);
                    $product->decrement('qty',$cart->qty);
                    // \DB::table('product_size')->where(['product_id'=>$cart->options->product_id, 'size_id'=>$cart->options->size_id])->decrement('qty',$cart->qty);
                    $model->transactionDetail()->create(
                        [
                            'name'=>$cart->name,
                            'price'=>$cart->price,
                            'qty'=>$cart->qty,
                            'price'=>$cart->price,
                            'product_id'=>$cart->options->product_id,
                            // 'size'=>$cart->options->size_id,
                            'discount'=>$cart->options->discount,
                        ]
                    );  
                }
            }
            Cart::destroy();
        });
       return $id;
    }

    public function show($id){
        $transaction = Transaction::with(['transactionDetail','user'])->find($id);
        $snapToken = null;
        if(Auth::user()->can('resi.update')){
            $transaction->setHasViewed();
        }
        return view('backend.store.transactions.show')
        ->with('transaction',$transaction);
    }
    

    public function notificationHandler(Request $request)
    {
        $notif = new \Midtrans\Notification();
        \DB::transaction(function() use($notif,$request) {
 
          $status = $notif->transaction_status;
          $type = $notif->payment_type;
          $orderId = $notif->order_id;
          $fraud = $notif->fraud_status;
          $transaction = Transaction::findOrFail($orderId);
 
          if ($status == 'capture') {
 
            // For credit card transaction, we need to check whether transaction is challenge by FDS or not
            if ($type == 'credit_card') {
 
              if($fraud == 'challenge') {
                // TODO set payment status in merchant's database to 'Challenge by FDS'
                // TODO merchant should decide whether this transaction is authorized or not in MAP
                // $transaction->addUpdate("Transaction order_id: " . $orderId ." is challenged by FDS");
                $transaction->setPending();
              } else {
                // TODO set payment status in merchant's database to 'Success'
                // $transaction->addUpdate("Transaction order_id: " . $orderId ." successfully captured using " . $type);
                $transaction->setSuccess();
              }
 
            }
 
          } elseif ($status == 'settlement') {
 
            // TODO set payment status in merchant's database to 'Settlement'
            // $transaction->addUpdate("Transaction order_id: " . $orderId ." successfully transfered using " . $type);
            $transaction->setSuccess();
 
          } elseif($status == 'pending'){
 
            // TODO set payment status in merchant's database to 'Pending'
            // $transaction->addUpdate("Waiting customer to finish transaction order_id: " . $orderId . " using " . $type);
            $transaction->setPending();
 
          } elseif ($status == 'deny') {
 
            // TODO set payment status in merchant's database to 'Failed'
            // $transaction->addUpdate("Payment using " . $type . " for transaction order_id: " . $orderId . " is Failed.");
            $transaction->setFailed();
 
          } elseif ($status == 'expire') {
 
            // TODO set payment status in merchant's database to 'expire'
            // $transaction->addUpdate("Payment using " . $type . " for transaction order_id: " . $orderId . " is expired.");
            $transaction->setExpired();
 
          } elseif ($status == 'cancel') {
 
            // TODO set payment status in merchant's database to 'Failed'
            // $transaction->addUpdate("Payment using " . $type . " for transaction order_id: " . $orderId . " is canceled.");
            $transaction->setFailed();
 
          }
          $transaction_payment = new \App\Models\TransactionPayment;
          $transaction_payment->transaction_id = $notif->order_id;
          $transaction_payment->transaction_time = $notif->transaction_time;
          $transaction_payment->transaction_status = $notif->transaction_status;
          $transaction_payment->store = $notif->store;
          $transaction_payment->payment_code = $notif->payment_code;
          $transaction_payment->payment_type = $notif->payment_type;
          $transaction_payment->order_id = $notif->order_id;
          $transaction_payment->gross_amount = $notif->gross_amount;
          $transaction_payment->currency = $notif->currency;
          $transaction_payment->save();
        });
 
        return;
    }

    public function getCarts(){
        return Cart::content();
    }

    public function cart(){
        $carts = Cart::content();
        return view('front_end.store.cart')
        ->withCarts($carts);
    }

    // public function cartNew(){
    //     $carts = Cart::content();
    //     return view('front_end.store.cart_')
    //     ->withCarts($carts);
    // }

    public function updateItem(Request $request,$rowId){
        $cart = Cart::get($rowId);
        $qty = $request->input('qty');
        $curQty = $cart->qty;
        $request->merge(['qty'=>$curQty+$qty]);
        // $request->merge(['qty'=>$qty]);
        $this->validate($request,[
            'qty'=>'required|numeric|max:'.$this->getCurrentQty($cart),
        ],[
            'qty.max'=>'Jumlah beli melebihi stok',
        ]);
        $tempQty = $curQty+$qty;
        // Cart::update($rowId, ($qty));
        Cart::update($rowId, ($tempQty));
    }

    public function getCurrentQty($item){
        // $product = Product::with('productImage','size')->findOrFail($item->id);
        // $stock = $product->size()->wherePivot('size_id','=',$item->options->size_id)->pluck('qty');
        // return count($stock) > 0 ? $stock[0] : 0;
        $product = Product::findOrFail($item->id);
        return $product->qty;
    }

    public function deleteItem($id){
        Cart::remove($id);
    }

    public function cekOngkir(Request $request){
        $origin      =$request->input('origin');
		$destination =$request->input('destination');
		$weight      =$request->input('weight');
		$courier     =$request->input('courier');
		$url         ='origin='.$origin.'&destination='.$destination.'&weight='.$weight.'&courier='.$courier;
    	return RajaOngkirHelper::postCost($url);	
    }

    public function invoice($id){
        $this->authorize('resi.update');
        $transaction = Transaction::with(['transactionDetail','user'])->find($id);
        $pdf = PDF::loadView('backend.store.transactions.invoice',['transaction'=>$transaction]);
        return $pdf->stream('invoice'.$transaction->id.'.pdf');
    }
    
    public function report(){
        $this->authorize('view.dashboard');
        $transactionType = ['unpaid','pending','success','cancel'];
        $tmp = [];
        foreach ($transactionType as $type) {
            array_push($tmp,Transaction::where('status',$type)->count());
        }
        $x = ['label'=>$transactionType,'data'=>$tmp];
        return response()->json($x);
    }

    public function reportByYear(Request $request){
        $this->authorize('view.dashboard');
        $year =$request->input('year') ?? date('Y');
        $month_name = [
            'January',
            'February',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        ];

        $tmp = [];
        $report = [];
        foreach ($month_name as $key => $value) {
            array_push($report,Transaction::whereYear('created_at',$year)->whereMonth('created_at',$key+1)->sum('total'));
        }
        $tmp = ['label'=>$month_name,'data'=>$report];
        return $tmp;
    }

    public function filter(Request $request){
        
    }
}
