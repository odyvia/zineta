<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use Illuminate\Support\Str;
use Auth,Storage,DB;
use App\Models\Product;
use App\Models\Category;
use ImageHelper;

class ProductController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except(['home','detail','getProducts']);
    }

    public function home(){
        $product = Product::with(['size','category','productImage'])->where('status','Y')->paginate(12);
        $categories = Category::all();
        return view('front_end.store.home')
        ->with('category',$categories)
        ->withProducts($product);
    }

    public function detail($id){
        $product = Product::with(['size','category','productImage'])->findOrFail($id);
        return view('front_end.store.view')
        ->withProduct($product);
    }



    public function index(){
        $products = Product::all();
        return view('backend.store.products.index')
        ->with('title','List Product')
        ->with('products',$products);
    }

    public function create(){
        return view('backend.store.products.create')
        ->withTitle('Tambah Produk');
    }

    public function store(ProductRequest $request){
       DB::transaction(function() use ($request){
        $model = new Product;
        $model->id = Str::uuid();
        $model->name = $request->input('name');
        $model->price = $request->input('price');
        $model->qty = $request->input('qty');
        $model->discount = $request->input('discount');
        $model->description = $request->input('description');
        $model->status = $request->input('status');
        $model->price = $request->input('price');
        $model->user_id = Auth::user()->id;
        $model->save();

        $category_id = $request->input('category');
        foreach($category_id as $category){
            $model->category()->attach($category);
        }

        // fungsi simpana relasi has many size

        // $sizes = json_decode($request->input('sizes'));
        // foreach ($sizes as $key => $size) {
        //     $model->size()->attach($size->size_id,['qty'=>$size->qty]);
        // }
        // fungsi simpan gambar

        foreach ($request->file('images') as $image) {
            $image_name = ImageHelper::setName($image->getClientOriginalName());
            $image_name_path = $image->storeAs('/public',$image_name);
            $model->productImage()->create([
                'name'=>$image_name
            ]);
        };
       });
    }

    public function destroy($id){

    }

    public function edit($id){
        $product = Product::with(['productImage','category'])->findOrFail($id);
        return view('backend.store.products.edit')
        ->withTitle('Edit Produk')
        ->withProduct($product->toJson());
    }

    public function update(ProductRequest $request,$id){
        $model = Product::with('productImage')->findOrFail($id);
        $model->name = $request->input('name');
        $model->price = $request->input('price');
        $model->qty = $request->input('qty');
        $model->discount = $request->input('discount');
        $model->description = $request->input('description');
        $model->price = $request->input('price');
        $model->status = $request->input('status');
        $model->user_id = Auth::user()->id;
        $model->save();

        // fungsi simpana relasi belongs to many size

        // $sizes = json_decode($request->input('sizes'));
        // foreach ($sizes as $key => $size) {
        //     $model->size()->updateExistingPivot($size->size_id,['qty'=>$size->qty]);
        // }


        // fungsi update category
        $category_id = $request->input('category');
        $model->category()->sync($category_id);
 
        // fungsi hapus image.jadi sebelum di update hapus dulu image terus input lagi
        foreach($model->productImage as $product_image){
            Storage::disk('local')->delete('public/'.$product_image->name);
        }
        $model->productImage()->delete();

        //fungsi save image
        foreach ($request->file('images') as $image) {
            $image_name = $image->getClientOriginalName();
            $image_name_path = $image->storeAs('public',$image_name);
            $model->productImage()->create([
                'name'=>$image_name
            ]);
        };
    }

    public function filter(Request $request){
        $product = Product::with(['size','category','productImage'])
        ->whereIn()
        ->where('status','Y')
        ->paginate(env('PAGINATE') ?? 12);
    }

    public function sort(){

    }

    public function getProducts(Request $request){
        $input_sort = $request->input('sort');
        $input_category = json_decode($request->input('input_category'));
        $products = Product::with(['size','category','productImage'])
        ->where('status','Y'); 
        if(count($input_category) > 0){
            $products = $products->whereHas('category',function($query) use ($input_category){
                return $query->whereIn('categories.id',$input_category);
            });
        }
        if($input_sort){
            $sort = explode(',',$request->input('sort'));
            $products->orderBy($sort[0],$sort[1]);
        }elseif ($input_sort == null) {
            $products->orderBy('created_at','desc');
        }
        
        return $products
        ->paginate(env('PAGINATE') ?? 12);
    }
}
