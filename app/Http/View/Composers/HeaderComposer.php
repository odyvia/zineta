<?php 
namespace App\Http\View\Composers;

use App\Models\Transaction;
use Illuminate\View\View;
use App\Models\Site;

class HeaderComposer
{
    public function compose(View $view)
    {
        $view->with('transactions',Transaction::where('has_viewed','N')->get());
        $view->with('site',Site::first());
    }
}