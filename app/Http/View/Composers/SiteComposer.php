<?php 
namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Models\Site;

class SiteComposer
{
    public function compose(View $view)
    {
        $view->with('site',Site::fisrt());
    }
}