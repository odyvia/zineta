<?php

namespace App\Policies;

use App\Models\Transaction;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function payment(User $user,Transaction $transaction){
        return $user->id === $transaction->user_id;
    }

    public function cancelPayment(User $user,Transaction $transaction){
        return $user->id === $transaction->user_id;
    }
}
