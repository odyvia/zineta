<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $tables='sizes';
    protected $primaryKey = 'id';
    protected $fillable=['name'];

    public function getSizeName($id){
        $size=Size::findOrFail($id);
        return $size->name;
    }
}
