<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table ='products';
    protected $primaryKey = 'id';
    protected $fillable = ['name','price','qty','description','user_id','image','discount','status'];
    public $incrementing = false;

    public function category(){
        return $this->belongsToMany('App\Models\Category');
    }

    public function productImage(){
        return $this->hasMany('App\Models\ProductImage');
    }

    public function size(){
        return $this->belongsToMany('App\Models\Size')->withPivot('qty');
    }

    public function productSize(){
        return $this->size->where('size_id',$this->size_id)->first();
    }

    public function scopeActive($query){
        return $query->where('status','Y');
    }

    public function scopeLowestStock($query){
        return $query->where('qty','<',5)->get();
    }



}
