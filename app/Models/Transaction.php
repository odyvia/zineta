<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use DateTime;
use \carbon\Carbon;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Transaction extends Model
{
    use SoftDeletes;
    
    public $incrementing = false;
    protected $tables = 'transactions';
    protected $primaryKey = 'id';
    protected $casts = [
        'origin_details'=> 'array',
        'destination_details'=>'array'
    ];

    public function retur(){

    }

    public function totalSales(){
        
    }

    public function checkExpiredTransaction(){
        $now = Carbon::now();
        $diff = $now->diffInSeconds($this->created_at->addHours(env('EXPIRED_DURATION')),false);
        if($diff <= 0 AND $this->status = 'unpaid'){
            foreach ($this->transactionDetail as $detail) {
                // \DB::table('product_size')->where(['product_id'=>$detail->product_id, 'size_id'=>$detail->size])->increment('qty',$detail->qty);
                \DB::table('product')->where('id',$detail->product_id)->decrement('qty',$detail->qty);
            }
            $this->delete();
        }
    }

    public function deleteExpiredTransaction(){
        // $transactions=Transaction::with('transactionDetail')
        // ->where('status','expired')
        // ->get();
        // $transaction->delete();
    }

    public function restoreStock($id,$qty){
        
    }

    public function transactionDetail(){
        return $this->hasMany('App\Models\TransactionDetail','transaction_id','id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function setPending()
    {
        $this->attributes['status'] = 'pending';
        self::save();
    }
 
    /**
     * Set status to Success
     *
     * @return void
     */
    public function setSuccess()
    {
        $this->attributes['status'] = 'success';
        self::save();
    }
 
    /**
     * Set status to Failed
     *
     * @return void
     */
    public function setFailed()
    {
        $this->attributes['status'] = 'failed';
        self::save();
    }
 
    /**
     * Set status to Expired
     *
     * @return void
     */
    public function setExpired()
    {
        $this->attributes['status'] = 'expired';
        self::save();
        foreach ($this->transactionDetail as $detail) {
            // \DB::table('product_size')->where(['product_id'=>$detail->product_id, 'size_id'=>$detail->size])->increment('qty',$detail->qty);
            $product = \App\Models\Product::findOrFail($detail->product_id);
            $product->increment('qty',$detail->qty);
        }
        $this->delete();
        
    }

    public function setSnapToken($value){
        $this->attributes['snap_token'] = $value;
        self::save();
    }

    public function setHasViewed(){
        $this->attributes['has_viewed'] = 'Y';
        self::save();
    }

    public function transactionPayment(){
        return $this->hasOne('App\Models\TransactionPayment','transaction_id','id');
    }

    // ScopeTransactionPending($query){
    //     return $query->whereHas('transactionPayment',function($q){
    //         return $q->transaction_status == 'pending';
    //     })
    // }

    // ScopeTransactionCancel($query){
    //     return $query->whereHas('transactionPayment',function($q){
    //         return $q->transaction_status == 'cancel';
    //     })
    // }

    // ScopeTransactionUnpaid($query){
    //     return $query->whereHas('transactionPayment',function($q){
    //         return $q->transaction_status == 'unpaid';
    //     })
    // }

    // ScopeTransactionSuccess($query){
    //     return $query->whereHas('transactionPayment',function($q){
    //         return $q->transaction_status == 'pending';
    //     })
    // }
}
