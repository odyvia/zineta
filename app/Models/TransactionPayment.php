<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionPayment extends Model
{
    protected $table='transaction_payment';
    protected $primaryKey='id';
    protected $fillable=[
        'transaction_time',
        'transaction_status',
        'transaction_id',
        'store',
        'payment_code',
        'payment_type',
        'order_id',
        'grosss_amount',
        'currency'
    ];

    public function transaction(){
        return $this->belongsTo(Transaction::class);
    }
}
