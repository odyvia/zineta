<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
   protected $table='categories';
   protected $fillable=['name'];
   protected $primaryKey='id';
   
   
   public function product(){
      return $this->belongsToMany('App\Models\Product');
   }
   
}
