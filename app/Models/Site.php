<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $table = 'sites';
    protected $primaryKey = 'id';

    public function carouselSite(){
        return $this->hasMany('App\Models\CarouselSite','site_id','id');
    }

}
