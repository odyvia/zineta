<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class TransactionDetail extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name',
        'qty',
        'size',
        'transaction_id',
        'product_id',
        'price',
        'size',
        'discount'
    ];
    protected $table ='transaction_details';
    

    public function transaction(){
        return $this->belongsTo('App\Models\Transaction');
    }

    public function product(){
        return $this->belongsTo('App\Models\Product','product_id','id');
    }
}
