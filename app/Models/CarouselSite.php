<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarouselSite extends Model
{
    protected $table = 'carousel_sites';
    protected $fillable=['image'];
    protected $primaryKey='id';
    
}
