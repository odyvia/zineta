<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuthProvider extends Model
{
    protected $fillable=['provider_id','user_id','provider'];
    public function user(){
        $this->hasMany(User::class);
    }
}
