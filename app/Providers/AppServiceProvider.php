<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Lookbook;
use App\Models\Product;
use App\Models\Transaction;
use App\Observers\LookbookObserver;
use App\Observers\ProductObserver;
use App\Observers\TransactionObserver;
use Illuminate\Pagination\Paginator;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        config(['app.locale' => 'id']);
        setlocale(LC_TIME, 'IND');
        date_default_timezone_set('Asia/Jakarta');
        Lookbook::observe(LookbookObserver::class);
        Product::observe(ProductObserver::class);
        Transaction::observe(TransactionObserver::class);
        Paginator::defaultView('vendor.pagination.bootstrap-4');
    }
}
