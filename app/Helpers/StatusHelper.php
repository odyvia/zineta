<?php

namespace App\Helpers;

class StatusHelper {
    public static function label($value){
        if($value === 'unpaid'){
            $label = '<span class="badge badge-secondary">Belum di Bayar</span>';
        }elseif ($value === 'success'){
            $label = '<span class="badge badge-success">Sudah di Bayar</span>';
        }elseif ($value === 'failed'){
            $label = '<span class="badge badge-danger">Gagal</span>';
        }elseif ($value === 'expired'){
            $label = '<span class="badge badge-light">Expired</span>';
        }elseif ($value === 'pending'){
            $label = '<span class="badge badge-warning">Pending</span>';
        }
        return $label;
    }
}