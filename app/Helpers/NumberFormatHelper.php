<?php

namespace App\Helpers;

class NumberFormatHelper {
    public static function idrFormat($value){
        return 'Rp '.number_format($value,0,',','.');
    }
}