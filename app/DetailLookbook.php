<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailLookbook extends Model
{
    protected $primaryKey = 'id';
    protected $fillable= ['image'];
}
