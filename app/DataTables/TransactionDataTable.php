<?php

namespace App\DataTables;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use App\Models\Transaction;
use App\Helpers\StatusHelper;
use Auth;

class TransactionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('status',function(Transaction $transaction){
                return StatusHelper::label($transaction->status);
            })
            ->rawColumns(['status','action'])
            ->addColumn('action', function(Transaction $transaction){
                 if(Auth::user()->can('resi.update' && $transaction->status == 'success')){ 
                    $action = "<a href=".url('transaction/'.$transaction->id).">
                    <button class='btn btn-sm btn-primary' >Lihat</button></a>
                    <button type='button' class='btn btn-sm btn-success' data-id=".$transaction->id." data-no_resi=".$transaction->no_resi." data-toggle='modal' data-target='#modal-resi'>
                    Update Resi
                  </button>";
                 }else{
                     $action ="<a href=".url('transaction/'.$transaction->id).">
                     <button class='btn btn-sm btn-primary' >Lihat</button></a>";
                 };
                 return $action;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\TransactionDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Transaction $model)
    {
        $max=$this->request->get('max') ?? \Carbon\Carbon::now()->endOfMonth()->toDateString();
        $min=$this->request->get('min') ?? \Carbon\Carbon::now()->startOfMonth()->toDateString();
        $status = $this->request->get('status');
        return $model->newQuery()->whereBetween('created_at',[$min,$max])->where(function($query) use ($status){
            if($status != ""){
                return $query->where('status',$status);
            }
        });
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('transactiondatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
           
            Column::make('name'),
            Column::make('status'),
            Column::make('created_at')->title('Tanggal'),
            Column::computed('action')
            ->exportable(false)
            ->printable(false)
            ->width(60)
            ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Transaction_' . date('YmdHis');
    }
}
