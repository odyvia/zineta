<?php

namespace App\Observers;

use App\Lookbook;
use Storage;

class LookbookObserver
{
    /**
     * Handle the lookbook "created" event.
     *
     * @param  \App\Lookbook  $lookbook
     * @return void
     */
    public function created(Lookbook $lookbook)
    {
        //
    }

    public function updating(Lookbook $lookbook){
        $details = $lookbook->detailLookbook;
        foreach($details as $detail){
            Storage::disk('local')->delete('public/'.$detail->image);
        }
        $lookbook->detailLookbook()->delete();
        
    }

    /**
     * Handle the lookbook "updated" event.
     *
     * @param  \App\Lookbook  $lookbook
     * @return void
     */
    public function updated(Lookbook $lookbook)
    {
        //
    }

    /**
     * Handle the lookbook "deleted" event.
     *
     * @param  \App\Lookbook  $lookbook
     * @return void
     */
    public function deleted(Lookbook $lookbook)
    {
        $details = $lookbook->detailLookbook;
        foreach($details as $detail){
            Storage::disk('local')->delete('public/'.$detail->image);
        }
        $lookbook->detailLookbook()->delete();
    }

    /**
     * Handle the lookbook "restored" event.
     *
     * @param  \App\Lookbook  $lookbook
     * @return void
     */
    public function restored(Lookbook $lookbook)
    {
        //
    }

    /**
     * Handle the lookbook "force deleted" event.
     *
     * @param  \App\Lookbook  $lookbook
     * @return void
     */
    public function forceDeleted(Lookbook $lookbook)
    {
        //
    }
}
