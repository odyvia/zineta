<?php

namespace App\Observers;
use APp\Models\Transaction;

class TransactionObserver
{
    public function deleted(Transaction $transaction)
    {
        $transaction->transactionDetail()->delete();
    }
}
