<?php

use Illuminate\Database\Seeder;
use App\Models\Size;
class SizeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Size::create([
            'name'=>'S',
            ]);
    }
}
