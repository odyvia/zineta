/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');
import Element from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css';
import numeral from 'numeral'
import  'datatables.net-responsive' 
import 'datatables.net-responsive-bs4'
import VueCurrencyFilter from 'vue-currency-filter'
import VueTinyLazyloadImg from 'vue-tiny-lazyload-img'
import 'bootstrap-datepicker/dist/css/bootstrap-datepicker.css'
require('bootstrap-datepicker');
window.Swal = require('sweetalert2');
Vue.use(Element, { locale });
Vue.filter("formatNumber", function (value) {
    return 'RP ' + numeral(value).format('0,0[.]00'); // displaying other groupings/separators is possible, look at the docs
});
Vue.use(VueCurrencyFilter,
    {
      symbol : 'Rp',
      thousandsSeparator: '.',
      fractionCount: 0,
      fractionSeparator: ',',
      symbolPosition: 'front',
      symbolSpacing: true
});
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.use(VueTinyLazyloadImg);



// let images = document.querySelectorAll(".lazyload");
// lazyload(images);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('shipment',require('./components/pages/Shipment.vue').default);

Vue.component('lookbook-create', require('./components/pages/lookbook/Create.vue').default);

Vue.component('product-create',require('./components/pages/product/Create.vue').default);

Vue.component('product-home',require('./components/pages/product/Home.vue').default);

Vue.component('cart',require('./components/pages/Cart.vue').default);

Vue.component('countdown',require('./components/Countdown.vue').default);

Vue.component('detail-lookbook',require('./components/pages/lookbook/DetailLookbook.vue').default);

// Vue.component('product-view',require('./components/pages/product/View.vue').default)

Vue.component('multiple-image-upload', require('./components/MultipleImageUpload.vue').default);

Vue.component('upload-image', require('./components/UploadImage.vue').default);

Vue.component('multi-input-file', require('./components/MultiInputFile.vue').default);

Vue.component('chart-transaction',require('./components/ChartTransaction.vue').default);

Vue.component('chart-total-transaction',require('./components/ChartTotalTransaction.vue').default);

Vue.component('count-cart',require('./components/CountCart.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
