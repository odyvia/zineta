@extends('backend.layouts.app')

@section('content')
<div class="card">
  <div class="card-header">
    Tambah Lookbook
  </div>
  <div class="card-body">
    <lookbook-create></lookbook-create>
  </div>
</div>
@endsection