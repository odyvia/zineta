@extends('backend.layouts.app')

@section('content')
     

<div class="card">
  <div class="card-header">
    Tambah Lookbook
  </div>
  <div class="card-body">
    <lookbook-create :prefill_lookbooks="{{ $lookbook }}"></lookbook-create>
  </div>
</div>
@endsection