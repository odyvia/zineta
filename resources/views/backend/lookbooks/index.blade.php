@extends('backend.layouts.app')

@section('content')
     

<div class="card">
  <div class="card-header">
    List Lookbook
  </div>
  <div class="card-body">
  <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Caption</th>
                <th>Image</th>
                <th>Create</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($lookbooks as $lookbook)
        <tr>
          <td>{{ $lookbook->caption }}</td>
          <td><img style="width:100px;height:100px" src="{{ asset('storage/'.$lookbook->image.'') }}" alt=""></td>
          <td>{{ $lookbook->created_at }}</td>
          <td><a href="{{ url('lookbook/'.$lookbook->id.'/edit') }}">
          <button class="btn btn-sm btn-info" >Edit</button></a>
          <form action="{{ url('lookbook/'.$lookbook->id) }}" method="POST">
          @method('delete')
          @csrf
          <button type="button" class="btn btn-sm btn-danger confirm-delete">Delete</button>
          </form>
          </td>
        </tr>
        @endforeach
        </tbody>   
    </table>
  </div>
</div>
@endsection

@push('scripts')
<script>
$(document).ready(function() {
    $('#example').DataTable();
});
</script>
@endpush