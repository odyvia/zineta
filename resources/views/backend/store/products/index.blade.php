@extends('backend.layouts.app')

@section('content')
     

<div class="card">
  <div class="card-header">
    {{ $title }}
  </div>
  <div class="card-body">
  <a href="{{ url('product/create') }}" class="btn btn-sm btn-primary">Tambah Produk</a>
  <div class="row">
    <div class="col-md-12">
      <div class="table-responsive">
      <table id="example" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Stok</th>
                <th>Harga</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $product->name }}</td>
          <td>{{ $product->qty }}</td>
          <td>{{ NumberFormat::idrFormat($product->price) }}</td>
          <td>{!! $product->status == 'Y' ? '<span class="badge badge-success">Aktif</span>' : '<span class="badge badge-danger">Tidak Aktif</span>' !!}</td>
          <td><a href="{{ url('product/'.$product->id.'/edit') }}">
          <button class="btn btn-sm btn-info" >Edit</button></a>
          
          </td>
        </tr>
        @endforeach
        </tbody>   
    </table>
      </div>
    </div>
  </div>
  </div>
</div>
@endsection

@push('scripts')
<script>
$(document).ready(function() {
    $('#example').DataTable();
});
</script>
@endpush