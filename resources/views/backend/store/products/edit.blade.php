@extends('backend.layouts.app')

@section('content')
<product-create :prefill-product="{{ $product }}"></product-create>
@endsection
