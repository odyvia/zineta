@extends('backend.layouts.app')

@section('content')
<form action="{{ route('category.store') }}" method="post">
    @csrf
    <div class="card">
        <div class="card-header">
            {{ $title ? $title : '' }}
        </div>
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="form-row">
                <label for="" class="col-3">Nama Kategori</label>
                <div class="col-6">
                    <input type="text" name="name" id="" class="form-control {{ $errors->first('name') ? 'is-invalid' : '' }}">
                    <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <input type="submit" class="btn btn-sm btn-info float-right" value="Simpan">
        </div>
    </div>
</form>
@endsection
