@extends('backend.layouts.app')

@section('content')
     

<div class="card">
  <div class="card-header">
    {{ $title ? $title : ''}}
  </div>
  <div class="card-body">
  @include('layouts._flash')
  <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Tanggal di Buat</th>
                <th>Tanggal di Edit</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
        <tr>
          <td>{{ $category->name }}</td>
          <td>{{ $category->created_at }}</td>
          <td>{{ $category->updated_at }}</td>
          <td><a href="{{ url('category/'.$category->id.'/edit') }}">
          <button class="btn btn-sm btn-info" >Edit</button></a>
         
          </td>
        </tr>
        @endforeach
        </tbody>   
    </table>
  </div>
</div>
@endsection

@push('scripts')
<script>
$(document).ready(function() {
    $('#example').DataTable();
});
</script>
@endpush