@extends('backend.layouts.app')

@section('content')
<div class="card">
          <div class="card-header">
              <strong>Tanggal</strong>
              <span class="float-right"> <strong>Status:</strong> {!! Status::label($transaction->status) !!}</span>

          </div>
          <div class="card-body">
              <div class="table-responsive-sm">
                  <table class="table table-striped">
                      <thead>
                          <tr>
                              <th class="center">#</th>
                              <th>Item</th>
                              <th class="right">Unit Cost</th>
                              <th class="center">Qty</th>
                              <th class="right">Total</th>
                          </tr>
                      </thead>
                      <tbody>
                      @php
                      $no = 0;
                      @endphp
                      @foreach($transaction->transactionDetail as $item)
                          <tr>
                              <td class="center">{{ $no+1 }}</td>
                              <td class="left strong">{{ $item->name }}</td>
                              <td class="right">{{ NumberFormat::idrFormat($item->price) }}</td>
                              <td class="center">{{ $item->qty }}</td>
                              <td class="right">{{ NumberFormat::idrFormat($item->price * $item->qty) }}</td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
              <div class="row">
                  <div class="col-lg-4 col-sm-5">
                    
                  </div>
                  <div class="col-lg-6 col-sm-6 ml-auto">
                      <table class="table table-clear">
                          <tbody>
                              <tr>
                                  <td class="left">
                                      <strong>Subtotal</strong>
                                  </td>
                                  <td class="right">{{ NumberFormat::idrFormat($transaction->total)  }}</td>
                              </tr>
                              <tr>
                                  <td class="left">
                                      <strong>{{ $transaction->courier }}</strong>
                                  </td>
                                  <td class="right">{{ NumberFormat::idrFormat($transaction->ongkir) }}</td>
                              </tr>
                              <tr>
                                  <td class="left">
                                      <strong>Total</strong>
                                  </td>
                                  <td class="right">
                                      <strong>
                                      {{ NumberFormat::idrFormat($transaction->ongkir + $transaction->total) }}</strong>
                                  </td>
                              </tr>
                          </tbody>
                      </table>
                   
                  </div>

              </div>
              @can('resi.update')
              <div class="float-right">
              <a href="{{ url('transaction/invoice/'.$transaction->id) }}" class="btn btn-sm btn-success justify-content-end">Print Invoice</a>
              @endcan
              </div>
          </div>
      </div>
@endsection