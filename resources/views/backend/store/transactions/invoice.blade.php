<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Aloha!</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }
    .gray {
        background-color: lightgray
    }
</style>

</head>
<body>

  <table width="100%">
    <tr>
        <td valign="top"><img src="{{asset('images/meteor-logo.png')}}" alt="" width="80"/></td>
        <td align="right">
            <h3>Zineta</h3>
            <pre>
                Alamat
                phone
                fax
            </pre>
        </td>
    </tr>

  </table>

  <table width="100%">
    <tr>
        <td width="100%"><strong>Pengirim :</strong></td>
        <td width="100%" align="right"><strong>Penerima :</strong></td>
    </tr>
    <tr>
        <td>zineta<br>jl moh toha<br>08123456789</td>
        <td align="right">{{ $transaction->name }} <br>
        {{ $transaction->address }} {{ $transaction->destination_details['city_name'] }} {{ $transaction->destination_details['province'] }} <br>
        {{ $transaction->phone }}<br>
        </td>
    </tr>
  </table>

  <br/>

  <table width="100%">
    <thead style="background-color: lightgray;">
      <tr>
        <th>#</th>
        <th>Description</th>
        <th>Quantity</th>
        <th>Unit Price $</th>
        <th>Total $</th>
      </tr>
    </thead>
    <tbody>
                      @php
                      $no = 0;
                      @endphp
                      @foreach($transaction->transactionDetail as $item)
                          <tr>
                              <td class="center">{{ $no+1 }}</td>
                              <td class="left strong">{{ $item->name }}</td>
                              <td class="right">{{ NumberFormat::idrFormat($item->price) }}</td>
                              <td class="center">{{ $item->qty }}</td>
                              <td class="right">{{ NumberFormat::idrFormat($item->price * $item->qty) }}</td>
                          </tr>
                          @endforeach
                      </tbody>

    <tfoot>
        <tr>
            <td colspan="3"></td>
            <td align="right">Subtotal</td>
            <td align="right">{{ NumberFormat::idrFormat($transaction->total)  }}</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td align="right">{{ $transaction->courier }}</td>
            <td align="right">{{ NumberFormat::idrFormat($transaction->ongkir) }}</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td align="right">Total</td>
            <td align="right" class="gray">{{ NumberFormat::idrFormat($transaction->ongkir + $transaction->total) }}</td>
        </tr>
    </tfoot>
  </table>

</body>
</html>