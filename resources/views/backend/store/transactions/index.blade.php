@extends('backend.layouts.app')

@section('content')

<div class="container">
    
<div class="card">
  <div class="card-header">
    {{ $title }}
  </div>
  <div class="card-body">
  <div class="form-group row">
  <div class="col-3">Tanggal Awal</div>
  <div class="col-2">
  <input class="form-control datepicker" value="{{ \Carbon\Carbon::now()->startOfMonth()->toDateString() }}" id="min" data-date-format="yyyy-mm-dd">
  </div>
  </div>

  <div class="form-group row">
  <div class="col-3">Tanggal Akhir</div>
  <div class="col-2">
  <input class="form-control datepicker" value="{{ \Carbon\Carbon::now()->endOfMonth()->toDateString() }}" id="max" data-date-format="yyyy-mm-dd">
  </div>
  </div>

  <div class="form-group row">
  <div class="col-3">Status</div>
  <div class="col-2">
 <select name="" id="status" class="form-control">
 <option value="">All</option>
 <option value="unpaid">Belum di bayar</option>
  <option value="pending">Pending</option>
  <option value="success">Sukses</option>
  <option value="cancel">Cancel</option></select>
  </div>
  </div>
  {{ $dataTable->table() }}
  </div>
</div>
</div>

<!-- Modal update resi -->
<!-- The Modal -->
<div class="modal" id="modal-resi">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Update No Resi</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <input type="text" id="no_resi" class="form-control" placeholder="No Resi"></input>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="save-resi" data-dismiss="modal">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<!-- end modal -->
@endsection
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/> -->

@push('scripts')
{{ $dataTable->scripts() }}
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script> -->
<script>
$(document).ready(function() {
  $('.datepicker').datepicker();
  $("#min,#max,#status").on('change',function(){
    window.LaravelDataTables["transactiondatatable-table"].draw();
  })

  $('#transactiondatatable-table').on('preXhr.dt', function ( e, settings, data ) {
        data.max = $('#max').val();
        data.min = $('#min').val();
        data.status = $("#status").val();
  });

  // $.fn.dataTable.ext.search.push(
  //       function (settings, data, dataIndex) {
  //           var min = $('#min').val();
  //           var max = $('#max').val();
  //           var startDate = (data[5]);
  //           if (min == null && max == null) { return true; }
  //           if (min == null && startDate <= max) { return true;}
  //           if(max == null && startDate >= min) {return true;}
  //           if (startDate <= max && startDate >= min) { return true; }
  //           return false;
  //       }
  //       );

       
  //           $("#min").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
  //           $("#max").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
  //           var table = $('#example').DataTable();

  //           // Event listener to the two range filtering inputs to redraw on input
  //           $('#min, #max').change(function () {
  //               table.draw();
  //           });

    $('#example').DataTable();
    $('#modal-resi').on('hide.bs.modal', function (event) {
      $("#no_resi").val("")
    });
    $('#modal-resi').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.data('id');
    var no_resi = button.data('no_resi');
    $("#no_resi").val(no_resi)
    $("#save-resi").click(function(val){
      $.ajax({
        type: "PUT",
        url: "updateresi/"+id,
        data: {
          "no_resi":$("#no_resi").val(),
          "_token": "{{ csrf_token() }}",
        },
        dataType: "text",
        success: function(resultData){
            $("#no_resi").val("")
            location.reload()
        }
    });
    })
  });
});
</script>
@endpush

