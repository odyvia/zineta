@extends('backend.layouts.app')

@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        <div class="row mb-5">
            <div class="col">
                <div class="card border-primary">
                    <div class="card-header">
                        Barang hampir habis
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <th>No</th>
                                <th>Nama Barang</th>
                                <th>Stok</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @if($lowestProduct->count() >0)
                                @php
                                $no = 0;
                                @endphp
                                @foreach($lowestProduct as $product)
                                <tr>
                                    <td>{{ $no+=1 }}</td>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->qty }}</td>
                                    <td><a href="{{ url('product/'.$product->id.'/edit') }}">
                    <button class='btn btn-sm btn-primary' >Lihat</button></a></td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card border-primary">
                    <div class="card-header">
                        User
                    </div>
                    <div class="card-body">
                        <h3 class="card-title"><i class="fa fa-user text-primary"></i> {{ App\User::count() }} </h3>
                        <p class="card-text">Total User</p>
                        
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card border-primary">
                    <div class="card-header">
                        Barang
                    </div>
                    <div class="card-body">
                        <h3 class="card-title"><i class="fa fa-shopping-bag text-primary"></i>
                            {{ App\Models\Product::active()->count() }} </h3>
                        <p class="card-text">Total Barang</p>
                        
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card border-primary">
                    <div class="card-header">
                        Transaksi
                    </div>
                    <div class="card-body">
                        <h3 class="card-title"><i class="fa fa-money-check-alt text-primary"></i> </h3>
                        <p class="card-text">Total Penjualan</p>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col">
                <div class="card border-primary mb-3">
                    <div class="card-header">Laporan Penjualan</div>
                    <div class="card-body text-primary">

                        <chart-transaction></chart-transaction>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card border-primary mb-3">
                    <div class="card-header">Laporan Penjualan</div>
                    <div class="card-body text-primary">

                        <chart-total-transaction></chart-total-transaction>
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>
@endsection