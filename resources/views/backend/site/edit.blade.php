@extends('backend.layouts.app')

@section('content')
<div class="container">
<div class="card">
{!! Form::model($site,["route"=>["site.update",$site->id],"method"=>"PUT","class"=>"form-horizontal","enctype"=>"multipart/form-data"]) !!}
    <div class="card-header">
        <strong>{{ $title }}</strong>
    </div>
    <div class="card-body">
        @include('backend.site._form')
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Simpan</button>
    </div>
    
    {!! Form::close() !!}
</div>
</div>
@endsection