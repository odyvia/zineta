<div class="form-group row">
    <label class="col-md-3 col-form-label" for="permission">Site Name</label>
    <div class="col-md-9">
        {!! Form::text('site_name',null,['class'=>$errors->has("site_name") ? "form-control is-invalid" : "form-control" ]) !!}
        <div class="invalid-feedback">
            {{ $errors->first("site_name") }}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="permission">Phone</label>
    <div class="col-md-9">
        {!! Form::text('phone',null,['class'=>$errors->has("phone") ? "form-control is-invalid" : "form-control" ]) !!}
        <div class="invalid-feedback">
            {{ $errors->first("phone") }}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="permission">Address</label>
    <div class="col-md-9">
        {!! Form::textarea('address',null,['class'=>$errors->has("address") ? "form-control is-invalid" : "form-control" ]) !!}
        <div class="invalid-feedback">
            {{ $errors->first("address") }}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="permission">Background Image</label>
    <div class="col-md-9">
        <!-- <input type="file" name="background_image" id="" value="{{ Storage::url($site->background_image) }}"> -->
        <!-- {{ Form::file('background_image') }} -->
        <upload-image prefill_url="{{ (Storage::url($site->background_image) ?? '') }}"></upload-image>
        <div class="invalid-feedback">
            {{ $errors->first("background_image") }}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="permission">Image Slidder</label>
    <div class="col-md-9">
        <multi-input-file images="{{ json_encode($site->carouselSite) }}"></multi-input-file>
        <div class="invalid-feedback">
            {{ $errors->first("name") }}
        </div>
    </div>
</div>