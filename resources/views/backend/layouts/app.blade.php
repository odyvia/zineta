<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pushop</title>
</head>

<link rel="stylesheet" href="{{ asset('css/backend/sb-admin-2.css') }}">
@stack('styles')
<body id="page-top">
<div id="app">
    <!-- Page Wrapper -->
  <div id="wrapper">

<!-- Sidebar -->
@include('backend.layouts.sidebar')
<!-- End of Sidebar -->

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
   @include('backend.layouts.topbar')
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">
    
      @yield('content')

    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->

  <!-- Footer -->
  @include('backend.layouts.footer')
  <!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
      <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
    </div>
    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
    <div class="modal-footer">
      <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
      <form action="{{ route('logout') }}" method="post">
      @csrf
      <button type="submit" class="btn btn-primary" href="login.html">Logout</button>
      </form>
    </div>
  </div>
</div>
</div>
</div>
  

  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('backend/js/sb-admin-2.js') }}"></script>
  
@stack('scripts')
</body>

</html>