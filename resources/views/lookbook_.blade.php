@extends('front_end.layouts.app')

@section('content')

<header>
    <div class="row">
        <div class="col-lg-6 offset-lg-3 text-center">
            <h2>PUSHOP</h2>
            <div class="text-wrap">
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa sit eius voluptatum at quasi tempora iusto illo nihil eum, 
                ratione ipsam aspernatur voluptatem atque nam ea perferendis aut corrupti velit!
            </div>
        </div>
    </div>
</header>

<section>
    <h2 class="text-center">OUR PRODUCT</h2>
    <div class="product-list">
        <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card shadow mb-1 product__card border-0">
                    <div class="card-body p-0">
                    <img src="{{asset('images/wb1.jpeg')}}" class="img-fluid" alt="gambar product">
                </div>
            </div>
                <button class="btn btn-primary mt-0 mb-3">Selengkapnya</button>
            </div>
            <div class="col-md-6">
                <div class="card shadow mb-1">
                    <div class="card-body p-0">
                    <img src="{{asset('images/wb1.jpeg')}}" class="img-fluid" alt="gambar product">
                </div>
            </div>
                <button class="btn btn-primary mt-0 mb-3">Selengkapnya</button>
            </div>
        </div>

        <div class="row justify-content-center mt-5">
             <div class="col-md-6">
                <div class="card shadow mb-1">
                    <div class="card-body p-0">
                    <img src="{{asset('images/wb1.jpeg')}}" class="img-fluid" alt="gambar product">
                </div>
            </div>
                <button class="btn btn-primary mt-0 mb-3">Selengkapnya</button>
            </div>
            
            <div class="col-md-6">
                <div class="card shadow mb-1">
                    <div class="card-body p-0">
                    <img src="{{asset('images/wb1.jpeg')}}" class="img-fluid" alt="gambar product">
                </div>
            </div>
                <button class="btn btn-primary mt-0 mb-3">Selengkapnya</button>
            </div>
        </div>
        </div>
    </div>
</section>

<section>
<div class="row">
        <div class="col-lg-6 offset-lg-3 text-center">
            <h2>Contact Us</h2>
            <div class="text-wrap">
                <h4>Anda ingin bertanya terkait produk kami ? </h4>
            </div>
            <div class="d-inline-flex flex-row">
                <a class="btn-wa">
                    <img src="{{asset('images/wa.png')}}" width="60px" alt=""> 
                    <p>Hubungi Kami</p>
                </a>
            </div>
        </div>
    </div>
</section>

<section>
<div class="row feature-boxes-container pt-2 justify-content-center">
					<div class="col-sm-6 col-lg-3">
						<div class="feature-box feature-box-simple text-center">
							<i class="icon-earphones-alt"></i>

							<div class="feature-box-content">
								<h3 class="text-uppercase">Customer Support</h3>
								<h5>Need Assistence?</h5>

								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec vestibulum magna, et dapib.</p>
							</div><!-- End .feature-box-content -->
						</div><!-- End .feature-box -->
					</div><!-- End .col-lg-3 -->

					<div class="col-sm-6 col-lg-3">
						<div class="feature-box feature-box-simple text-center">
							<i class="icon-credit-card"></i>

							<div class="feature-box-content">
								<h3 class="text-uppercase">Secured Payment</h3>
								<h5>Safe &amp; Fast</h5>

								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec vestibulum magna, et dapibus lacus. Lorem ipsum dolor sit amet.</p>
							</div><!-- End .feature-box-content -->
						</div><!-- End .feature-box -->
					</div><!-- End .col-lg-3 -->

					<div class="col-sm-6 col-lg-3">
						<div class="feature-box feature-box-simple text-center">
							<i class="icon-shipping"></i>

							<div class="feature-box-content">
								<h3 class="text-uppercase">Free Shipping</h3>
								<h5>Orders Over Rp 400.000</h5>

								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec vestibulum magna, et dapib.</p>
							</div><!-- End .feature-box-content -->
						</div><!-- End .feature-box -->
					</div>

					
				</div>
</section>

@endsection