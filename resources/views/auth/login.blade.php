@extends('front_end.layouts.app')
@section('content')
@include('front_end.layouts.header_')
<div class="container">
            @include('layouts._flash')
        </div>
<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Login</h1>
                  </div>
                  <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4 form-footer mb-0">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="forget-pass" href="{{ route('password.request') }}">
                                    Forgot Your Password
                                </a>
                            </div>
                        </div>
                       
                        <div class="mb-4">
                            <hr data-content="OR" class="hr-text">
                        </div>
                       
                        <div class="form-group row d-flex justify-content-center">
                            <div class="col text-center">
                                <a href="{{ url('/auth/google') }}" class="btn btn-google"><i class="fab fa-google"></i> Google</a>
                                <a href="{{ url('/auth/facebook') }}" class="btn btn-facebook"><i class="fab fa-facebook-f"></i> Facebook</a>
                            </div>
                        </div>
                    </form>

                    <p class="text-center">Doesn't have an account? <a href="{{ url('/register') }}">Register</a></p>
                
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
  
@endsection 
