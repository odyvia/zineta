@extends('front_end.layouts.app')

@section('content')

@include('front_end.layouts.header')
<div id="app">

<h3 class="text-center">{{ $lookbook->caption }}</h3>

<div class="container">
<detail-lookbook :images="{{ $lookbook->detailLookbook->toJson() }}"></detail-lookbook>
    <!-- <div class="row">
       @foreach($lookbook->detailLookbook as $detail)
       <div class="col-12 m-3">
            <figure class="figure">
            <img class="img-fluid" src="http://www.camera-m.com/img/placeholder-image-wide.png" data-src="{{ asset('storage/'.$detail->image.'') }}" alt="">
            </figure>
        </div>
       @endforeach
    </div> -->
</div>

</div>
@include('front_end.layouts.footer')

@endsection

@push('scripts')

@endpush