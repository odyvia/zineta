@extends('front_end.layouts.app')

@section('content')
@include('front_end.layouts.header_')
<div id="app" class="container">
<center>
@if($transaction->status === 'expired')
<div class="status-tag expired">Expired</div>

            @else
            <div id="timer" class="timer">
                <!--  Timer Component  -->
                <countdown starttime="{{ $transaction->created_at }}"
                    endtime="{{ $transaction->created_at->addHours(env('EXPIRED_DURATION',24)) }}" trans='{  
         "day":"Day",
         "hours":"Hours",
         "minutes":"Minuts",
         "seconds":"Seconds",
         "expired":"Transaksi sudah kadaluarsa",
         "running":"Silahkan Lakukan Pembayaran sebelum waktu berakhir",
         "upcoming":"Till start of event.",
         "status": {
            "expired":"Expired",
            "running":"Running",
            "upcoming":"Future"
           }}'></countdown>
                <!--  End! Timer Component  -->
            </div>            
            @endif
            </center>    
<div class="card">
        <div class="card-header">
            <strong>{{ $transaction->created_at }}</strong>
            <span class="float-right"> <strong>Status:</strong> {!! Status::label($transaction->status)
                !!}</span>

        </div>
        <div class="card-body">
            

            <div class="row mb-4">
                @php
                $destination = ($transaction->destination_details);
                @endphp

                <div class="col-sm-6">
                    <h6 class="mb-3">To:</h6>
                    <div>
                        <strong>{{ $transaction->name }}</strong>
                    </div>
                    <div>{{ $transaction->user->address }}</div>
                    <div>{{ $transaction->address }} {{ $destination['city_name']}} {{ $destination['province'] }}
                        {{ $destination['postal_code'] }}</div>
                    <div>{{ $transaction->user->email }}</div>
                    <div>{{ $transaction->phone }}</div>
                </div>

                <div class="col-sm-1"></div>

                @if($transaction->transaction_payment)
                <div class="col-sm">
                    <h6 class="mb-3">Status Pembayaran</h6>
                    <div>
                        <strong>{{ $transaction->transactionPayment->transaction_status ?? '' }}</strong>
                    </div>
                    <div>Jenis Pembayaran :{{ $transaction->transactionPayment->store ?? '' }}</div>
                    <div><strong>Kode Pembayaran :{{ $transaction->transactionPayment->payment_code ?? '' }}</strong></div>
                </div>
                @endif



            </div>

            <div class="table-responsive-sm">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="center">#</th>
                            <th>Item</th>
                            <th class="right">Unit Cost</th>
                            <th class="center">Qty</th>
                            <th class="right">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($transaction->transactionDetail as $item)
                        <tr>
                            <td class="center">1</td>
                            <td class="left strong">{{ $item->name }}</td>
                            <td class="right">Rp {{ number_format($item->price,0,',','.') }}</td>
                            <td class="center">{{ $item->qty }}</td>
                            <td class="right">{{ number_format($item->price * $item->qty ,0,',','.') }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-5">

                </div>

                <div class="col-lg-4 col-sm-5 ml-auto">
                    <table class="table table-clear">
                        <tbody>
                            <tr>
                                <td class="left">
                                    <strong>Subtotal</strong>
                                </td>
                                <td class="right">Rp {{ number_format($transaction->total,'0',',','.')  }}</td>
                            </tr>
                            <tr>
                                <td class="left">
                                    <strong>{{ $transaction->courier }}</strong>
                                </td>
                                <td class="right">Rp {{ number_format($transaction->ongkir,'0',',','.') }}</td>
                            </tr>
                            <tr>
                                <td class="left">
                                    <strong>Total</strong>
                                </td>
                                <td class="right">
                                    <strong>Rp
                                        {{ number_format($transaction->ongkir + $transaction->total,'0',',','.') }}</strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>

            </div>
            @if($transaction->status == 'unpaid')
            <div class="float-right">
            <button id="btn-payment" class="btn btn-primary" onclick="pay('{{ $snap_token }}')">Bayar</button>
            &nbsp;
            <button id="cancel-payment" class="btn btn-danger">Batalkan Transaksi</button>
            </div>
            @endif
        </div>
    </div>
</div>
@include('front_end.layouts.footer')
@endsection
@push('scripts')
<script>
$(document).ready(function(){
    $("#cancel-payment").click(function(){
        Swal.fire({
        title: 'Anda Yakin akan membatalkan transaksi?',
        text: "Barang belanjaan anda akan di hapus",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Batalkan Transaksi!'
        }).then((result) => {
        if (result.value) {
            $.ajax({
                url:'/cancelpayment/{{ $transaction->id}}',
                type:'PUT',
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success:function(data){
                    window.location.href='/transaction_status';
                    },
                error:function(error){
                    console.log(error)
                }
                });
            }
        })
    });
});

var start = new Date('{{ $transaction->created_at }}').getTime();
var end = new Date('{{ $transaction->created_at->addHours(env('
    EXPIRED_DURATION ')) }}').getTime();

$(document).ready(function(){
    timerCount(start,end);
});
function timerCount(start, end) {
    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = start - now;
    var passTime = end - now;

    if (distance < 0 && passTime < 0) {
        $("#btn-payment").prop('disabled', true);
        return;

    }
}
function calcTime(dist) {
    // Time calculations for days, hours, minutes and seconds
    this.days = Math.floor(dist / (1000 * 60 * 60 * 24));
    this.hours = Math.floor((dist % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    this.minutes = Math.floor((dist % (1000 * 60 * 60)) / (1000 * 60));
    this.seconds = Math.floor((dist % (1000 * 60)) / 1000);
}

function pay(data) {
    snap.pay(data, {
        // Optional
        onSuccess: function (result) {
            window.location.href='/transaction_status';
        },
        // Optional
        onPending: function (result) {
            window.location.href='/transaction_status';
        },
        // Optional
        onError: function (result) {
            window.location.href='/transaction_status';
        }
    });
}
</script>
@endpush
