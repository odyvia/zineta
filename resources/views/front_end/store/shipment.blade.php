@extends('front_end.layouts.app')

@include('front_end.layouts.header_')
@section('content')
    <div class="page-header">
        <div class="container">
            <h1>Checkout</h1>
        </div><!-- End .container -->
    </div>

<div id="app" class="container">
    <shipment></shipment>
</div>
@include('front_end.layouts.footer')

@endsection
