@extends('front_end.layouts.app')

@section('content')
@include('front_end.layouts.header_')

@if(Cart::count() == 0)
<center>
<img class="img-fluid" src="{{ asset('images/empty-cart.png') }}" alt="" srcset="">
<br>
<a href="{{ url('/') }}"><button class="btn btn-shop btn-lg">Lanjutkan Belanja</button></a>
</center>

@else
<!-- <div id="app">
<cart></cart>
</div> -->
<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{url('store')}}">Home</a></li>
						<li class="breadcrumb-item"><a href="#">Cart</a></li>
					</ol>
				</div><!-- End .container -->
			</nav>

			<div class="page-header">
				<div class="container">
					<h1>Shopping Cart</h1>
</div>
</div>

    <!-- <div class="container cart-header">
        <div class="row mt-5 text-center">
            <div class="col">
                <h3>Your Cart</h3>
            </div>
        </div>
    </div> -->

    <div class="container">
				<div class="row row-sparse">
					<div class="col-lg-8 padding-right-lg">
						<div class="cart-table-container">
							<table class="table table-cart">
								<thead>
									<tr>
										<th class="product-col">Product</th>
										<th class="price-col">Price</th>
										<th class="qty-col">Qty</th>
										<th>Subtotal</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach($carts as $item)
									<tr class="product-row pr-{{ $item->rowId }}">
										<td class="product-col">
											<figure class="product-image-container">
												<a href="product.html" class="product-image">
													<img src="{{ Storage::url($item->options->image) }}" alt="product">
												</a>
											</figure>
											<h3 class="product-title">
												<a href="#">{{ $item->name }}</a>
											</h3>
										</td>
										<td>{{ NumberFormat::idrFormat($item->price) }}</td>
										<td>

                                            <input class="vertical-quantity form-control" type="text" value="{{$item->qty}}" data-id="{{ $item->rowId }}">
											<!-- <div class="input-group  bootstrap-touchspin bootstrap-touchspin-injected">
                                                <span class="input-group-btn-vertical">
                                                    <button class="btn btn-outline bootstrap-touchspin-up icon-up-dir" type="button"></button>
                                                    <button class="btn btn-outline bootstrap-touchspin-down icon-down-dir" type="button"></button>
                                                </span>
                                            </div> -->
										
										</td>
										<td class="subtotal">{{ NumberFormat::idrFormat($item->price * $item->qty) }}</td>
                                        <tr class="product-action-row pr-{{ $item->rowId }}">
										<td colspan="4" class="clearfix">
											<div class="float-left">
												<!-- <a href="#" class="btn-move">Move to Wishlist</a> -->
											</div><!-- End .float-left -->
											
											<div class="float-right">
												<!-- <a href="#" title="Edit product" class="btn-edit"><span class="sr-only">Edit</span><i class="icon-pencil"></i></a> -->
												<a href="#" title="Remove product" data-id="{{ $item->rowId }}" class="btn-remove icon-cancel"><span class="sr-only">Remove</span></a>
											</div><!-- End .float-right -->
										</td>
									</tr>
									</tr>
                                    @endforeach
								</tbody>

								<tfoot>
									<tr>
										<td colspan="4" class="clearfix">
											<div class="float-left">
												<a href="category.html" class="btn btn-outline-secondary">Lanjut Belanja</a>
											</div><!-- End .float-left -->

											<div class="float-right">
												<a href="#" class="btn btn-outline-secondary btn-clear-cart">Clear Shopping Cart</a>
											</div><!-- End .float-right -->
										</td>
									</tr>
								</tfoot>
							</table>
						</div><!-- End .cart-table-container -->
					</div><!-- End .col-lg-8 -->


                    
          <div class="col-lg-4">
						<div class="cart-summary">
							<h3 class="text-center">Informasi Biaya</h3>

							<table class="table table-totals">
								<tbody>
                                    @foreach($carts as $value)
									<tr class="pr-{{ $value->rowId }}">
										<td>{{$value->name}}</td>
										<td>{{ NumberFormat::idrFormat($value->qty * $value->price) }}</td>
									</tr>
                                    @endforeach
								</tbody>
								<tfoot>
									<tr>
										<td>Total Harga</td>
										<td> </td>
									</tr>
								</tfoot>
							</table>

							<div class="checkout-methods">
								<a href="/shipment" class="btn btn-block btn-sm btn-primary">Pembayaran</a>
							</div><!-- End .checkout-methods -->
						</div><!-- End .cart-summary -->
					</div><!-- End .col-lg-4 -->
				</div><!-- End .row -->
			</div><!-- End .container -->

		
  <!-- Modal -->
  <div class="modal fade checkout-modal-success" id="checkoutModal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body text-center">
          <img src="img/api/cart/sukses_checkout.png" class="mb-5">
          <h3>Checkout Berhasil</h3>
          <p>Anda akan mendapatkan barang anda <br> dalam beberapa hari</p>
          <button type="button" class="btn mt-3" style="background-color: #EAEAEF; color: #ADADAD;"
            data-dismiss="modal">Home</button>
        </div>
      </div>
    </div>
  </div>
				</div><!-- End .container -->
			</div><!-- End .page-header -->

			
@endif

@include('front_end.layouts.footer')
@endsection
@push('scripts')
<script>
    function toNumber(curr){
        return curr.split(' ')[1].replace(/\./g,'')*1;
    }

    
    $(document).ready(function(){
        let carts;
        let total;
        async function getCarts(){
          let url = '/api/carts';
          let response = await fetch(url);
          return response.json();
        }

        function loadCart(){
            getCarts().then(data=>{
                console.log(data);
                carts = data;
                total = Object.keys(carts).map(t=>{ return carts[t].subtotal }).reduce((sum,val)=>sum + val,0);
            })
        }

        loadCart();

        // let total = ()=>{
        //     let x= Object.keys(carts).map(t=>{ return carts[t].subtotal });
        // 	return x.reduce((sum,val)=>sum + val,0);
        // }
        // function cart(){

        //     let data = response.data;
        //       carts = data;
        //       console.log(carts);
        // }
        let _token = $('meta[name="csrf-token"]').attr('content');

        $('.vertical-quantity').change(function(){
            console.log(carts);
            let baris = $(this).closest('tr');
            let price = baris.find('td:nth-child(2)').text();
            let subtotal = baris.find('td:nth-child(4)');
            let newPrice = toNumber(price);
            let id  = $(this).attr('data-id');
            subtotal.text(newPrice*$(this).val());
            let url="/api/cart/"+id+"/update";
            let qty = $(this).val();
            let tmpQty;
            // let curQty = this.carts[rowId].qty;
            // tmpQty = parseInt(curQty) + parseInt(qty);
            loadCart();
            $.ajax({
                method:'POST',
                url:url,
                data:{
                    _token,
                    rowId:id,
                    qty:qty
                },
                success:function(res){
                    console.log(total);
                    // console.log(carts);
                },
                error:function(err){
                    let errMessage = err.responseJSON.errors.qty[0];
                    console.log(err.responseJSON.errors.qty[0]);
                }
            });
        })

        $('.btn-remove').click(function(){
            let id = $(this).attr('data-id');
            let url="/api/cart/"+id+"/delete";
            let baris = $('table').find(`tr.pr-${id}`);
            $.ajax({
                method:'DELETE',
                url:url,
                data:{
                    _token,
                    rowId:id
                },
                success:function(res){
                    baris.remove();
                    
                },
                errors:function(err){
                    console.log(err);
                }

            })
        })
    })

</script>
@endpush

