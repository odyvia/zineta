@extends('front_end.layouts.app')

@include('front_end.layouts.header')
@section('content')
<div class="container mt-5">
    <nav>
        <ol class="breadcrumb bg-transparent pl-0">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Status</li>
        </ol>
    </nav>
</div>
<center>
<img class="img-fluid mb-5" src="{{ asset('images/sukses_checkout.png') }}" alt="" srcset="">
<h3 style="margin-bottom:50px;">Transaksi Anda Berhasil</h3>
<a href="{{ url('/') }}" style=""><button class="btn btn-shop btn-lg">Lanjutkan Belanja</button></a>
</center>


@include('front_end.layouts.footer')
@endsection

<style>
.btn-shop{
    background-color:#000 !important;
    color:#ffffff !important;
    border-radius:0 0 0 0 !important;
    border-color:#000 !important;
}
</style>

