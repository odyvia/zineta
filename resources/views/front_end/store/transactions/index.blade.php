@extends('front_end.store.profile')
@section('profile-content')
<div class="container">
<div class="h-100">
<div class="card">
  <div class="card-header">
    {{ $title }}
  </div>
  <div class="card-body">
  <table id="example" class="table table-striped table-bordered display responsive nowrap" width="100%">
        <thead>
            <tr>
                <th>No Order</th>
                <th>Nama</th>
                <th>Total</th>
                <th>Kurir</th>
                <th>Status Pembayaran</th>
                <th>No Resi</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($transactions as $transaction)
        <tr>
          <td>{{ $transaction->invoice }}</td>
          <td>{{ $transaction->user->name }}</td>
          <td>{{ NumberFormat::idrFormat($transaction->total + $transaction->ongkir) }}</td>
          <td>{{ $transaction->courier }}</td>
          <td>{!! Status::label($transaction->status) !!}</td>
          <td>{{ $transaction->no_resi }}</td>
          <td><a href="{{ url('payment/'.$transaction->id) }}">
          @can('payment',$transaction)
          <button class="btn btn-sm btn-primary" >Lihat</button></a>
          @endcan
          </form>
          </td>
        </tr>
        @endforeach
        </tbody>   
    </table>
  </div>
</div>
</div>
</div>

@endsection

@push('scripts')
<script>
$(document).ready(function() {
    $('#example').DataTable({
      responsive: true
    });
    
});
</script>
@endpush