@extends('front_end.layouts.app')
@section('content')
        @include('front_end.layouts.header_')
		<!-- End .header -->
		<div id="app">
			<product-home></product-home>
		</div>
		
		<!-- End .main -->

		@include('front_end.layouts.footer')
@endsection 
@push('scripts')

@endpush
