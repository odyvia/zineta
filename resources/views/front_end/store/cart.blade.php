@extends('front_end.layouts.app')

@include('front_end.layouts.header_')
@section('content')

<count-cart></count-cart>
@if(Cart::count() == 0)
<center>
<img class="img-fluid" src="{{ asset('images/empty-cart.png') }}" alt="" srcset="">
<br>
<a href="{{ url('/') }}"><button class="btn btn-shop btn-lg">Lanjutkan Belanja</button></a>
</center>

@else
<!-- <div id="app">
<cart></cart>
</div> -->
<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item"><a href="#">Cart</a></li>
					</ol>
				</div><!-- End .container -->
			</nav>

			<div class="page-header">
				<div class="container">
					<h1>Shopping Cart</h1>
					<cart></cart>
				</div><!-- End .container -->
			</div><!-- End .page-header -->

			
@endif

@include('front_end.layouts.footer')
@endsection

<style>
.btn-shop{
    background-color:#000 !important;
    color:#ffffff !important;
    border-radius:0 0 0 0 !important;
    border-color:#000 !important;
}
</style>

