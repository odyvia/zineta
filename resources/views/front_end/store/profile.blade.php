@extends('front_end.layouts.app')
@section('content')
        @include('front_end.layouts.header_')
		<!-- End .header -->
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
					</ol>
				</div><!-- End .container -->
			</nav>

			<div class="container">
				<div class="row row-sparse">
					<div class="col-lg-9 order-lg-last dashboard-content">
						@yield('profile-content')
					</div><!-- End .col-lg-9 -->

					<aside class="sidebar col-lg-3">
						<div class="widget widget-dashboard">
							<h3 class="widget-title">My Account</h3>

							<ul class="list">
								<li><a href="#">Dashboard</a></li>
								<li><a href="#">Info Akun</a></li>
								<li @if(Route::is('mytransaction')) class="active" @endif><a href="{{ url('/mytransaction') }}">Transaksi</a></li>
								<li><a href="#">Riwayat Transaksi</a></li>
							</ul>
						</div><!-- End .widget -->
					</aside><!-- End .col-lg-3 -->
				</div><!-- End .row -->
			</div><!-- End .container -->

			<div class="mb-5"></div><!-- margin -->
		</main><!-- End .main -->

		@include('front_end.layouts.footer')
@endsection 
@push('scripts')

@endpush
