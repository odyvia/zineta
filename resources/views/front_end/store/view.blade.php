@extends('front_end.layouts.app')
@section('content')
@include('front_end.layouts.header_')
<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('/store') }}"><i class="icon-home"></i></a></li>
						<li class="breadcrumb-item"><a href="#">Product</a></li>
						<li class="breadcrumb-item active" aria-current="page">Detail</li>
					</ol>
				</div><!-- End .container -->
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 main-content">
						<div class="product-single-container product-single-default">
							<div class="row">
								<div class="col-lg-5 col-md-6 product-single-gallery">
									<div class="product-slider-container">
										<div class="product-single-carousel owl-carousel owl-theme">
                                        @foreach($product->productImage as $key => $value)
											<div class="product-item">
												<img class="product-single-image" src="{{ Storage::url($value->name) }}" data-zoom-image="{{ Storage::url($value->name) }}"/>
											</div>
                                        @endforeach
										</div>
										<!-- End .product-single-carousel -->
										<span class="prod-full-screen">
											<i class="icon-plus"></i>
										</span>
									</div>
									<div class="prod-thumbnail owl-dots" id='carousel-custom-dots'>
                                    @foreach($product->productImage as $key => $value)
										<div class="owl-dot owl-detail">
											<img src="{{ Storage::url($value->name) }}"/>
										</div>
                                    @endforeach
									</div>
								</div><!-- End .col-lg-7 -->

								<div class="col-lg-5 col-md-6 product-single-details">
									<h1 class="product-title">{{$product->name}}</h1>


									<hr class="short-divider">
									<div class="price-box">
                                        @if($product->discount > 0)
                                        <span class="old-price">{{ NumberFormat::idrFormat($product->price) }}</span>
										<span class="product-price text-danger">{{ NumberFormat::idrFormat($product->price - ($product->price * $product->discount / 100)) }}</span>
                                        @else
                                        <p class="product-price">{{ NumberFormat::idrFormat($product->price) }}</p>
                                        @endif
									</div><!-- End .price-box -->

									<div class="product-desc">
										<p>
                                        {{ $product->description }}
                                        </p>
									</div><!-- End .product-desc -->

								
									<hr class="divider">

									<div class="product-action">
                                    <form action="{{ route('addtocart',['id'=>$product->id]) }}" method="POST">
                                        @csrf
										<div class="product-single-qty">
											<input class="horizontal-quantity form-control" name="qty" type="number" min="1">
										</div><!-- End .product-single-qty -->

										<button type="submit" class="btn btn-dark add-cart" title="Add to Cart">Add to Cart</button>
                                    </form>
									</div><!-- End .product-action -->

									<hr class="divider mb-1">
                                    <div class="product-single-share">
										<label class="sr-only">Share:</label>

										<div class="social-icons mr-2">
											<a href="#" class="social-icon social-facebook icon-facebook" target="_blank" title="Facebook"></a>
											<a href="#" class="social-icon social-whatsapp fab fa-whatsapp " target="_blank" title="Twitter"></a>
											<a href="#" class="social-icon social-linkedin icon-instagram" target="_blank" title="Instagram"></a>
											<a href="#" class="social-icon social-gplus fab fa-google-plus-g" target="_blank" title="Google +"></a>
										</div><!-- End .social-icons -->

									</div><!-- End .product single-share -->

								</div><!-- End .product-single-details -->
							</div><!-- End .row -->
						</div><!-- End .product-single-container -->

						<div class="product-single-tabs">
							<ul class="nav nav-tabs" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="product-tab-desc" data-toggle="tab" href="#product-desc-content" role="tab" aria-controls="product-desc-content" aria-selected="true">Description</a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane fade show active" id="product-desc-content" role="tabpanel" aria-labelledby="product-tab-desc">
									<div class="product-desc-content">
										<p>{{ $product->description }}</p>
									</div><!-- End .product-desc-content -->
								</div><!-- End .tab-pane -->

							</div><!-- End .tab-content -->
						</div><!-- End .product-single-tabs -->
					</div><!-- End .main-content -->

                    @php

$categoryIds = $product->category->pluck('id')->toArray();

$similarProducts = \App\Models\Product::with('productImage')->where('status','Y')->whereHas('category', function ($query) use ($categoryIds) {
    return $query->whereIn('category_id', $categoryIds);
})
    ->where('id', '!=' ,$product->id)
    ->limit(4)
    ->get();
@endphp

					<div class="sidebar-overlay"></div>
					<div class="sidebar-toggle"><i class="fas fa-sliders-h"></i></div>
					<aside class="sidebar-product col-lg-3 mobile-sidebar">
						<div class="sidebar-wrapper">
							<div class="widget widget-info">
								<ul>
									<li>
										<i class="icon-shipping"></i>
										<h4>FREE<br>SHIPPING</h4>
									</li>
									<li>
										<i class="icon-us-dollar"></i>
										<h4>100% MONEY<br>BACK GUARANTEE</h4>
									</li>
									<li>
										<i class="icon-online-support"></i>
										<h4>ONLINE<br>SUPPORT 24/7</h4>
									</li>
								</ul>
							</div><!-- End .widget -->

							<div class="widget">
								<a href="#">
									<img src="{{asset('images/banner-sidebar.jpg')}}" class="w-100" alt="Banner Desc">
								</a>
							</div><!-- End .widget -->

							<div class="widget widget-featured">
								<h3 class="widget-title">Featured</h3>
								
								<div class="widget-body">
									<div class="owl-carousel widget-featured-products">
										<div class="featured-col">
                                        @foreach($similarProducts as $similar_product)
											<div class="product-default left-details product-widget">
												<figure>
                                                    <a href="{{ route('product.detail',$similar_product->id) }}">
                                                        <img src="{{ Storage::url($similar_product->productImage()->first()->name) }}">
                                                    </a>
												</figure>
												<div class="product-details">
													<h5 class="product-title">
                                                    <a href="{{ url('product/$similar_product->id/view') }}">{{ $similar_product->name }}</a>
													</h5>
													<div class="ratings-container">
														<div class="product-ratings">
															<span class="ratings" style="width:100%"></span><!-- End .ratings -->
															<span class="tooltiptext tooltip-top"></span>
														</div><!-- End .product-ratings -->
													</div><!-- End .ratings-container -->
													<div class="price-box">
                                                            @if($similar_product->discount > 0)
                                                            <span class="old-price">{{ NumberFormat::idrFormat($similar_product->price) }}</span>
                                                            <span class="product-price text-danger">{{ NumberFormat::idrFormat($similar_product->price - ($similar_product->price * $similar_product->discount / 100)) }}</span>
                                                            @else
                                                            <p class="product-price">{{ NumberFormat::idrFormat($similar_product->price) }}</p>
                                                            @endif
                                                    </div><!-- End .price-box -->
												</div><!-- End .product-details -->
											</div>
                                        @endforeach
										</div><!-- End .featured-col -->
									</div><!-- End .widget-featured-slider -->
								</div><!-- End .widget-body -->
							</div><!-- End .widget -->
						</div>
					</aside><!-- End .col-lg-3 -->
				</div><!-- End .row -->
            </div>
                <!-- Button trigger modal -->

<!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm modal-dialog-centered">
                    <div class="modal-content">
                    <!-- <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div> -->
                    <div class="modal-body">
                        @if(session('success'))
                            <h2 class="text-center">
                                {{ session('success') }}
                            </h2>
                        @endif
                    </div>
                    </div>
                </div>
                </div>
			</div><!-- End .container -->
           
			<div class="products-section">
				<div class="container">
					<h2>Related Products</h2>
                    <div class="products-slider owl-carousel owl-theme dots-top">
                        @if(count($similarProducts) > 0)
                        @foreach($similarProducts as $similar_product)
						<div class="product-default inner-quickview inner-icon p-2 detail">
							<figure>
								<a href="{{ route('product.detail',$similar_product->id) }}">
									<img src="{{ Storage::url($similar_product->productImage()->first()->name) }}">
								</a>
                                <a href="#" class="btn-quickview" title="Quick View">Quick View</a> 
								<div class="label-group">
                                    @if($similar_product->diskon > 0 )
									<span class="product-label label-sale">{{ $similar_product->diskon }}%</span>
                                    @endif
								</div>
								<div class="btn-icon-group">
									<button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-shopping-cart"></i></button>
								</div>
							</figure>
							<div class="product-details">
								<div class="category-wrap">
									<div class="category-list">
										<a href="#" class="product-category">
                                            @foreach($similar_product->category as $catName)
                                                {{ $catName->name }}
                                            @endforeach
                                        </a>
									</div>
								</div>
								<h3 class="product-title">
									<a href="{{ url('product/$similar_product->id/view') }}">{{ $similar_product->name }}</a>
								</h3>
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:100%"></span><!-- End .ratings -->
                                        <span class="tooltiptext tooltip-top"></span>
                                    </div><!-- End .product-ratings -->
                                </div>
								
								<div class="price-box">
                                         @if($similar_product->discount > 0)
                                        <span class="old-price">{{ NumberFormat::idrFormat($similar_product->price) }}</span>
										<span class="product-price text-danger">{{ NumberFormat::idrFormat($similar_product->price - ($similar_product->price * $similar_product->discount / 100)) }}</span>
                                        @else
                                        <p class="product-price">{{ NumberFormat::idrFormat($similar_product->price) }}</p>
                                        @endif
								</div><!-- End .price-box -->
							</div><!-- End .product-details -->
						</div>
                        @endforeach
                        
                        {{-- <div class="col mb-4">
                            <div class="card h-100">
                            <img src="{{ Storage::url('1624942247-wb1.jpeg') }}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                            </div>
                            </div>
                        </div> --}}

                        @else
                        <p class="text-center">Tidak Ada Produk Terkait</p>
                        @endif
					</div><!-- End .products-slider -->
				</div><!-- End .container -->
			</div><!-- End .products-section -->
		</main> 

 
@include('front_end.layouts.footer')

@endsection

@push('scripts')

    @if(session('success'))
        <script>
            $('#exampleModal').modal('show');

            $('#exampleModal').on('shown.bs.modal', function (event) {
                setTimeout(() => {
                    $('#exampleModal').modal('hide');
                }, 1500);
            })
        </script>
    @endif
<script>
    var numImgs = $('div.secondary-images .secondary-image').length;
    var imageClicked;
    var clickedImage;
    var qty=0;
    function increment(){
        let x = $("#qty").val();
        x++;
        if(x > 0){
            $("#qty").val(x);
        }
    }

    function decrement(){
        let x = $("#qty").val();
        x--;
        if(x > 0){
            $("#qty").val(x);
        }
    }

    $('.secondary-image').click(function () {
        var prevImages = $(this).prevAll().length;

        if (prevImages > 0) {
            $('.lightbox-controls-previous').show();
        } else {
            $('.lightbox-controls-previous').hide();
        }
        if (prevImages == (numImgs - 1)) {
            $('.lightbox-controls-next').hide();
        } else {
            $('.lightbox-controls-next').show();
        }
    });

    $('.thumb').click(function (event) {
        // console.log($(this))
        let imageViewer = new ImageViewer('.image-viewer');
        clickedImage = $(this);
        imageViewer.setMainImage(event)
        imageViewer.setLightboxImage(event)
    })

    $(document).ready(() => {
        $('div.secondary-image.active').find('img').trigger('click');
        console.log($('div.secondary-image.active'))
    })

    $('.lightbox-controls-next').click(function () {
        $(clickedImage).closest('.secondary-image').next().find('img').trigger('click');
    })

    $('.lightbox-controls-previous').click(function () {
        $(clickedImage).closest('.secondary-image').prev().find('img').trigger('click');
    })
    class ImageViewer {
        constructor(selector) {
            var clickedImage;
            this.selector = selector;
            $(this.mainImage).click(() => this.showLightbox(event));
            $(this.lightboxClose).click(() => this.hideLightbox(event));
        }

        get secondaryImageSelector() {
            return '.secondary-image';
        }

        get mainImageSelector() {
            return '.main-image';
        }

        get lightboxImageSelector() {
            return '.lightbox';
        }

        get lightBoxControlsNext() {
            return '.lightbox-controls-next';
        }

        get lightboxClose() {
            return '.lightbox-controls-close';
        }

        get secondaryImages() {
            var secondaryImages = $(this.selector).find(this.secondaryImageSelector).find('img');
            return secondaryImages;
        }

        get mainImage() {
            var mainImage = $(this.selector).find(this.mainImageSelector);
            return mainImage;
        }

        get lightboxImage() {
            var lightboxImage = $(this.lightboxImageSelector);
            return lightboxImage;
        }

        get nextLightBoxControlsImage() {
            var nextImage = $(this.lightBoxControlsNext);
            return nextImage;
        }


        setNextImage(event) {
            $(event.target).closest('.secondary-image').next().find('img').trigger('click');
        }

        setLightboxImage(event) {
            var src = this.getEventSrc(event);
            this.setSrc(this.lightboxImage, src);
        }

        setMainImage(event) {
            var src = this.getEventSrc(event);
            $(event.target).parent().addClass('active').siblings().removeClass('active');
            this.setSrc(this.mainImage, src);
        }

        getSrc(node) {
            var image = $(node).find('img');
        }

        setSrc(node, src) {
            var image = $(node).find('img')[0];
            image.src = src;
        }

        getEventSrc(event) {
            return event.target.src;
        }

        showLightbox(event) {
            this.setLightboxImage(event);
            $(this.lightboxImageSelector).addClass('show');
        }

        hideLightbox() {
            $(this.lightboxImageSelector).removeClass('show');
        }
    }


    new ImageViewer('.image-viewer');

</script>
@endpush
