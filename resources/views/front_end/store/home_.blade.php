@extends('front_end.layouts.app')
@section('content')
        @include('front_end.layouts.header_')
		<!-- End .header -->
		<!-- <div id="app">
			<product-home></product-home>
		</div> -->
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav mb-md-4">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
					</ol>
				</div><!-- End .container -->
			</nav>

			<div class="container mb-3">
				<div class="row row-sparse">
					<div class="col-lg-9 main-content">
						<div class="category-banner">
							<img class="slide-bg" src="https://via.placeholder.com/728x90?text=728x90+Banner" alt="banner" width="0" height="0">
						</div><!-- End .category-slide -->

						<nav class="toolbox">
							<div class="toolbox-left">
								<div class="toolbox-item toolbox-sort">
									<label>Sort By:</label>

									<div class="select-custom">
										 <select name="" id="" class="form-control filter">
                                            <option value="null" disabled hidden>Sortir</option>
                                            <option value="created_at,desc">Terbaru</option>
                                            <option value="price,asc">Termurah</option>
                                            <option value="price,desc">Termahal</option>
                                        </select>
									</div><!-- End .select-custom -->

									
								</div><!-- End .toolbox-item -->
							</div><!-- End .toolbox-left -->

							<div class="toolbox-right">
								<div class="toolbox-item toolbox-show">
									<label>Show:</label>

									<div class="select-custom">
										<select name="count" class="form-control">
											<option value="20">20</option>
											<option value="30">30</option>
											<option value="40">40</option>
											<option value="50">50</option>
										</select>
									</div><!-- End .select-custom -->
								</div><!-- End .toolbox-item -->
							</div><!-- End .toolbox-right -->
						</nav>
                
                    
                    <div class="row justify-content-around justify-content-sm-start">      
							@foreach($products as $product)
							<div class="col-6 col-sm-4 col-md-3 col-xl-5col" style="max-width:48%; //border:1px solid black; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);">
								<div class="product-default inner-quickview inner-icon">
									<figure>
										<a href="{{route('product.detail',$product->id)}}">
											<img 
                                            data-src="{{Storage::url($product->productImage[0]->name)}}"
                                            src="{{Storage::url($product->productImage[0]->name)}}"
                                            >
										</a>
										@if($product->discount > 0)
										<div class="label-group">
											<span class="product-label label-sale">{{ $product->discount }}%</span>
										</div>
										@endif
									</figure>

									<div class="product-details">
										<h2 class="product-title">
											<a href="{{route('product.detail',$product->id)}}">{{ $product->name }}</a>
										</h2>
										<div class="ratings-container">
											
										</div><!-- End .product-container -->
										<div class="price-box">
										@if($product->discount > 0)
                                            <div>
                                                <p class="old-price" style="margin-bottom:7px;">{{ $product->price }}</p>
                                                <span class="product-price" style="color:red;">{{ $product->price - ($product->price * $product->discount / 100) }}</span>
                                            </div>
										@else
                                            <div>
												<span class="product-price">{{ $product->price }}</span>
                                            </div>
										</div><!-- End .price-box -->
										@endif
									</div><!-- End .product-details -->
								</div>
							</div>
							@endforeach
						</div>

						@if(count($products) == 0)
                        <div>
                            <h3 class="text-center mt-2">Produk Tidak Ada</h3>
                        </div>
						@endif
                	</div>

						<nav class="toolbox toolbox-pagination">
							<div class="toolbox-item toolbox-show">
								
							</div><!-- End .toolbox-item -->

							<!-- <pagination :data="products" @pagination-change-page="getResults"></pagination> -->
						</nav>
					</div><!-- End .main-content -->

					<div class="sidebar-overlay"></div>
					<div class="sidebar-toggle"><i class="fas fa-sliders-h"></i></div>
					<aside class="sidebar-shop col-lg-3 order-lg-first mobile-sidebar">
						<div class="sidebar-wrapper">
							<div class="widget">
								<h3 class="widget-title">
									<a data-toggle="collapse" href="#widget-body-2" role="button" aria-expanded="true" aria-controls="widget-body-2">Categories</a>
								</h3>

								<div class="collapse show" id="widget-body-2">
									<div class="widget-body">
										<ul class="cat-list">
											@foreach($category as $itemCat)
											 <li>
                                                <input type="checkbox"
                                                    value="{{$itemCat->id}}" class="category" name="" id=""> {{ $itemCat->name }}
                                            </li>
											@endforeach
										</ul>
									</div><!-- End .widget-body -->
								</div><!-- End .collapse -->
							</div><!-- End .widget -->
		

						</div><!-- End .sidebar-wrapper -->
					</aside><!-- End .col-lg-3 -->

				</div><!-- End .row -->
			</div><!-- End .container -->
		</main><!-- End .main -->
		<!-- End .main -->

		@include('front_end.layouts.footer')
@endsection 
@push('scripts')
<script>
	$(document).ready(function(){
		let catFilter = [];
		let sortir;
		let product;
		async function getProduct(){
			let res = await $.ajax({
				url:'/api/products',
				data:{
					sort:sortir,
					input_category:JSON.stringify([])
				},
				success:function(res){
					let prod;
					prod = res.data;
					return prod;
				}
			});
			return res;
		}
		$('.filter').change(function(){
			sortir = $(this).val();
			console.log(sortir);
			getProduct().then(data=>console.log(data));
			// console.log(product);
		})
	})
</script>
@endpush
