<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Pushop</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/my.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fonts/vendor/fontawesome/css/all.min.css') }}">
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css"> -->
    <script type="text/javascript">
		WebFontConfig = {
			google: { families: [ 'Open+Sans:300,400,600,700,800','Poppins:300,400,500,600,700' ] }
		};
		(function(d) {
			var wf = d.createElement('script'), s = d.scripts[0];
			wf.src = `{{asset ('js/webfont.js')}}`;
			wf.async = true;
			s.parentNode.insertBefore(wf, s);
		})(document);
	</script>
  
    <style>
body {
        font-family: 'Montserrat';
    }

.main-nav .menu>li>a ,.header-middle{
  color:black !important;
}

/* .sidebar-toggle{
  padding-top:10px;
} */
.status-tag {
    width: 270px;
    margin: 10px auto;
    padding: 8px 0;
    font-weight: 500;
    color: 
    #fff;
    text-align: center;
    border-radius: 15px;
}

.status-tag.expired {
    background-color: 
    red;
}

.btn-wa{
  margin-top: 10px;
  display:inline-block;
  padding:10px;
  background-color: green;
  color:white;
  font-weight:bold;
  border-radius:5%;
}

</style>
</head>
<body>
  <div id="app" class="page-wrapper">
    @yield('content')
  </div>
<div class="mobile-menu-overlay"></div><!-- End .mobil-menu-overlay -->
    @include('front_end.layouts.sidebar-mobile')
<a id="scroll-top" href="#top" title="Top" role="button"><i class="icon-angle-up"></i></a>
</body>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/plugins.min.js') }}" defer></script>
	<script src="{{ asset('js/main.min.js') }}" defer></script>

@stack('scripts')
</html>