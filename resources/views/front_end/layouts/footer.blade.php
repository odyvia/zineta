<footer class="footer">
		<hr>
			<div class="footer-middle">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-xl-4">
							<div class="widget">
								<h4 class="widget-title">Contact Info</h4>

								<div class="row">
									<div class="col-sm-6">
										<div class="contact-widget">
											<h4 class="widget-title">ADDRESS</h4>
											<a href="#">1234 Street Name, City, England</a>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="contact-widget email">
											<h4 class="widget-title">EMAIL</h4>
											<a href="mailto:mail@example.com">mail@example.com</a>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="contact-widget">
											<h4 class="widget-title">PHONE</h4>
											<a href="#">Toll Free (123) 456-7890</a>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="contact-widget">
											<h4 class="widget-title">WORKING DAYS/HOURS</h4>
											<a href="#">Mon - Sun / 9:00 AM - 8:00 PM</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-lg-4 col-xl-4">
							<div class="widget">
								<h4 class="widget-title">My Account</h4>
								<ul class="links link-parts row">
									<div class="link-part col-xl-4">
										<li><a href="/about">About Us</a></li>
										<li><a href="#">Contact Us</a></li>
										<li><a href="#">My Account</a></li>
									</div>
									<div class="link-part col-xl-8">
										<li><a href="#">Orders History</a></li>
									</div>
								</ul>
							</div><!-- End .widget -->
						</div>
					</div>
				</div>
			</div>

			<div class="footer-bottom">
				<div class="container top-border d-flex align-items-center justify-content-between flex-wrap">
					<p class="footer-copyright mb-0 py-3 pr-3">Pushop. &copy;  2021.  All Rights Reserved</p>
				</div>
			</div>
		</footer>