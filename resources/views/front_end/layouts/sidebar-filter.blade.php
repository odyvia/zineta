<aside class="sidebar-shop col-lg-3 order-lg-first mobile-sidebar">
						<div class="sidebar-wrapper">
							<div class="widget">
								<h3 class="widget-title">
									<a data-toggle="collapse" href="#widget-body-2" role="button" aria-expanded="true" aria-controls="widget-body-2">Categories</a>
								</h3>

								<div class="collapse show" id="widget-body-2">
									<div class="widget-body">
										<ul class="cat-list">
											<li><a href="#">Accessories</a></li>
											<li><a href="#">Watch Fashion</a></li>
											<li><a href="#">Tees, Knits &amp; Polos</a></li>
											<li><a href="#">Pants &amp; Denim</a></li>
										</ul>
									</div><!-- End .widget-body -->
								</div><!-- End .collapse -->
							</div><!-- End .widget -->
		

						</div><!-- End .sidebar-wrapper -->
					</aside><!-- End .col-lg-3 -->