<!-- navbar on phone ony -->
<div class="d-block d-sm-none">
<nav class="navbar navbar-light bg-light navbar-expand-md justify-content-between" style="background-color:white !important;z-index:5;">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-nav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse dual-nav w-50 order-1 order-md-0">
            <ul class="navbar-nav">
            <li class="nav-item">
        <a class="nav-link {{ setActive(['/']) }}" href="{{ url('/') }}">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ setActive(['lookbook']) }}" href="{{ url('lookbook') }}">Look Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ setActive(['about']) }}" href="{{ url('about') }}">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ setActive(['mytransaction']) }}" href="{{ url('mytransaction') }}">Riwayat Transaksi</a>
      </li>
            </ul>
        </div>
        <a href="{{ url('/') }}" class="navbar-brand mx-auto d-block text-center order-0 order-md-1 w-25 zineta-logo">zineta</a></h1>
        <a class="nav-link" style="color:black" href="{{ url('cart') }}"><i class="fas fa-shopping-bag"></i>
                @if(Cart::count() > 0)
                  <span class="badge badge-danger badge-counter">{{ Cart::count() }}</span>
                @endif
                </a>
        <div class="navbar-collapse collapse dual-nav w-50 order-2">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link" href="">Cart<i class="fas fa-shopping-bag"></i>
                @if(Cart::count() > 0)
                  <span class="badge badge-danger badge-counter">{{ Cart::count() }}</span>
                @endif
                </a>
                </li>
                @if(Auth::check())
                 <a class="nav-link" href="#" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Logout
          </a>
          @else
          <a class="nav-link" href="{{ route('login') }}">Login</a>
          @endif
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
            </ul>
        </div>
    </div>
</nav>

</div>

<nav class="navbar navbar-expand-lg navbar-expand-md navbar-expand-sm d-none d-sm-block navbar-light bg-light" id="top-navbar" style="background-color:white !important;">
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      @if(!Auth::check())
      <li class="nav-item active">
        <a class="nav-link" href="{{ url('login') }}">Login <span class="sr-only">(current)</span></a>
      </li>
      @else
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {{ Auth::user()->name }}
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{ url('/profile/'.Auth::user()->id.'/edit') }}">Edit Profile</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ url('mytransaction') }}">Riwayat Transaksi</a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              Logout
          </a>

        <form id="logout-form" action="#" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
        </div>
      </li>
      @endif
      @if(!Auth::check())
      <li class="nav-item">
        <a class="nav-link" href="{{ url('register') }}">Sign Up</a>
      </li>
      @endif
      <li class="nav-item">
        <a class="nav-link" href="{{ url('cart') }}">Cart
        @if(Cart::count() > 0)
        <span class="badge badge-danger badge-counter">{{ Cart::count() }}</span>
        @endif
        </a>
      </li>
      <li class="nav-item">
      <a class="navbar-brand" href="#">
    
      <img src="{{ asset('images/ig.png') }}" style="width:20px;height:20px" alt="">
  </a>
      </li>
    </ul>
  </div>
</nav>

<nav class="navbar navbar-light bg-light d-none d-sm-block" style="background-color:white !important;" id="mid-navbar">
<ul class="navbar-nav mx-auto">
      <center>
      <li class="nav-item active">
        <a class="nav-link" href="{{ url('/') }}"><h1>{{ ($site->site_name) ?? '' }}</h1> <span class="sr-only">(current)</span></a>
      </li>
      </center>
</ul>
</nav>

<nav class="navbar navbar-expand-lg d-none d-sm-block navbar-expand-md navbar-expand-sm navbar-light bg-light" id="low-navbar" style="z-index:3;!important;background-color:white !important;">
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mx-auto">
      <li class="nav-item">
        <a class="nav-link {{ setActive(['/']) }}" href="{{ url('/') }}">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="htpp://shop.zineta.id">Shop</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ setActive(['lookbook']) }}" href="{{ url('lookbook') }}">Look Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ setActive(['about']) }}" href="{{ url('about') }}">About</a>
      </li>
    </ul>
  </div>
</nav>

@push('scripts')
<script>
              
// When the user scrolls the page, execute myFunction
window.onscroll = function() {myFunction()};

// Get the header
var header = document.getElementById("low-navbar");

// Get the offset position of the navbar
var sticky = header.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("fixed-top");
    header.style.borderBottom='1px solid #e5e5e5';
  } else {
    header.classList.remove("fixed-top");  
    header.style.borderBottom='';
  }
} 

</script>
@endpush