<div class="mobile-menu-container">
		<div class="mobile-menu-wrapper">
			<span class="mobile-menu-close"><i class="icon-compare-link"></i></span>
			<nav class="mobile-nav">
				<ul class="mobile-menu">
					<li class="active"><a href="{{ url('/') }}">Home</a></li>
					<li>
                        <a class="" href="htpp://shop.zineta.id">Shop</a>
                    </li>
                    <li>
                        <a class=" {{ setActive(['lookbook']) }}" href="{{ url('lookbook') }}">Look Book</a>
                    </li>
                    <li>
                        <a class=" {{ setActive(['about']) }}" href="{{ url('about') }}">About</a>
                    </li>
				</ul>
			</nav><!-- End .mobile-nav -->

			<div class="social-icons">
				<a href="#" class="social-icon" target="_blank"><i class="icon-facebook"></i></a>
				<a href="#" class="social-icon" target="_blank"><i class="icon-twitter"></i></a>
				<a href="#" class="social-icon" target="_blank"><i class="icon-instagram"></i></a>
			</div><!-- End .social-icons -->
		</div><!-- End .mobile-menu-wrapper -->
	</div><!-- End .mobile-menu-container -->