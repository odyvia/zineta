<header class="header">
			<div class="header-middle sticky-header">
				<div class="container">
					<div class="header-left">
                        <a href="{{ url('/store') }}" class="logo">
                            <h3>{{ config('app.name','PuShop') }}</h3>
						</a>
						<nav class="main-nav font2">
							<ul class="menu">
                            <!-- <li>
                                </li> -->
                                <li>
                                    <a class=" {{ setActive(['/']) }}" href="{{ url('/') }}">Home</a>
                                </li>
                                <li>
                                    <a class="" href="{{ url('store') }}">Shop</a>
                                </li>
                                <li>
                                    <a class=" {{ setActive(['lookbook']) }}" href="{{ url('lookbook') }}">Look Book</a>
                                </li>
                                <li>
                                    <a class=" {{ setActive(['about']) }}" href="{{ url('about') }}">About</a>
                                </li>
							</ul>
						</nav>
					</div><!-- End .header-left -->

					<div class="header-right">
						<button class="mobile-menu-toggler" type="button">
							<i class="icon-menu"></i>
						</button>

						<div class="header-search header-search-popup header-search-category d-none d-sm-block">
							<a href="#" class="search-toggle" role="button"><i class="icon-magnifier"></i></a>
							<form action="#" method="get">
								<div class="header-search-wrapper">
									<input type="search" class="form-control" name="q" id="q" placeholder="I'm searching for..." required="">
									<div class="select-custom">
										<select id="cat" name="cat">
											<option value="">All Categories</option>
											<option value="4">Tas</option>
											<option value="12">Hoodie</option>
											<option value="13">Waist Bag</option>
										</select>
									</div><!-- End .select-custom -->
									<button class="btn bg-dark icon-search-3" type="submit"></button>
								</div><!-- End .header-search-wrapper -->
							</form>
						</div>
						<div class="dropdown cart-dropdown">

							<a href="{{ url('/cart') }}" class="header-icon">
									<i class="icon-shopping-cart"></i>
									@if(Cart::count() > 0)
										<span class="cart-count badge-circle mr-4">{{ count(Cart::content()) }}</span>
									@endif
								</a>
						</div>
						
                        @if(Auth::check())
							<div class="dropdown cart-dropdown">
								<a href="#" class="header-icon dropdown-toggle dropdown-arrow" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static"><i class="icon-user-2"></i></a>
									<div class="dropdown-menu">
										<div class="dropdownmenu-wrapper">
											<h5>Hii , {{Auth::user()->name}}</h5>
											<div class="dropdown-cart-action">
												<div class="row">
													<div class="col mb-2">
														<a class="btn btn-primary btn-block" href="{{url('/profile/'.Auth::user()->id.'/edit')}}" class="dropdown-item">
															Profile
														</a>
													</div>
													<div class="w-100"></div>
													<div class="col">
														<a class="btn btn-primary btn-block" href="#" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
															Logout
														</a>
													</div>
												</div>
											</div>
										</div>
										
									</div>
								</div>
                        @else
                            <a class="nav-link" href="{{ route('login') }}">Login</a>
                        @endif
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form> 
					</div><!-- End .header-right -->
				</div><!-- End .container -->
			</div><!-- End .header-middle -->
		</header><!-- End .header -->

