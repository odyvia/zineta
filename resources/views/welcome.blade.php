@extends('front_end.layouts.app')

@section('content')

@include('front_end.layouts.header')
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100 img-fluid" src="{{ asset('images/1.jpg') }}" alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('images/2.jpg') }}" alt="Second slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('images/3.jpg') }}" alt="Third slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>


<div class="row">
    <div class="col" style="height:100px">

    </div>
</div>


<section class="lookbook">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col text-center mx-auto">
            <a href="{{ url('lookbook') }}" class="btn m-5 btn-lg btn-lookbook">
                concept lookbook
            </a>
        </div>
    </div>
</section>

<div class="container justify-content-center mt-5">
<div class="row">
{{-- @php
$i=0;
@endphp
@foreach($items->data as $item)
@if($i < 4)
<div class="col-12 col-xs-6 col-sm-6 col-lg-3 mt-1">
<figure class="snip0015">
<img class="img-fluid w-100" style="object-fit: cover;height:255px" src="{{ $item->images->low_resolution->url }}" alt="">
<figcaption>
		<p>{{ $item->caption->text }}</p>
		<a href="{{ $item->link }}" target="_blank"></a>
	</figcaption>
</figure>
    </div>
@php
$i++;
@endphp
@endif
@endforeach --}}
</div>
</div>


@include('front_end.layouts.footer')

@endsection
