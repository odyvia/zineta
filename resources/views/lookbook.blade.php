@extends('front_end.layouts.app')

@section('content')

@include('front_end.layouts.header_')

<section class="lookbook-page">
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
        {{-- @foreach($lookbooks as $lookbook)
        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-g-6">
                <div class="lookbook-image mb-5">
                    <div class="card">
                        <img class="card-img" src="{{ asset('storage/'.$lookbook->image.'') }}" alt="...">
                        <a class="d-flex justify-center" href="{{ url('lookbook/'.$lookbook->id.'/show') }}">
                        </a>
                        <div class="card-img-overlay text-white d-flex justify-content-center align-items-end">
                            <h1>{{ $lookbook->caption }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach --}}
        <h3 class="text-center">Coming Soon</h3>
    </div>
</section>

@include('front_end.layouts.footer')

@endsection