<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::domain('store.zineta.test')->group(function () {
    Route::resource('category','CategoryController');
    Route::resource('product','ProductController');
    Route::get('/store','ProductController@home');
    Route::get('product/{id}/view','ProductController@detail')->name('product.detail');
    Route::view('/coba', 'front_end.store.product-view');
    Route::view('/','lookbook_');
    Route::post('addtocart/{id}','TransactionController@addToCart')->name('addtocart');
    Route::get('cart','TransactionController@cart')->name('cart');
    // Route::get('cartNew','TransactionController@cartNew')->name('cartNew');
    Route::POST('checkout','TransactionController@checkout');
    Route::get('shipment','TransactionController@shipment');
    Route::get('payment/{id}','TransactionController@payment');
    Route::put('cancelpayment/{id}','TransactionController@cancelPayment');
    Route::get('mytransaction','TransactionController@myTransaction')->name('mytransaction');
    Route::get('transaction','TransactionController@index');
    Route::get('transaction/{id}','TransactionController@show');
    Route::get('transaction/report/status','TransactionController@report');
    Route::get('transaction/report/year','TransactionController@reportByYear');
    Route::get('check','TransactionController@setExpiredTransaction');
    Route::PUT('updateresi/{id}','TransactionController@updateResi');
    Route::resource('role','RoleController');
    Route::resource('permission','PermissionController');
    Route::resource('user','UserController');
    Route::get('profile/{id}/edit','UserController@editProfile');
    Route::get('transaction/invoice/{id}','TransactionController@invoice');
    Route::PUT('profile/{id}','UserController@updateProfile')->name('update.profile');
    Route::get('site/edit','SiteController@edit');
    Route::PUT('site/update','SiteController@update');
    Route::resource('site','SiteController');
    Route::get('transaction_status',function(){
        return View::make('front_end.store.success');
    });
    Route::view('/profile','front_end.store.profile');
    Route::get('dashboard','SiteController@dashboard')->name('dashboard');
    //khusus api
    Route::prefix('api')->group(function(){
        Route::get('cities','CityController@getCities');
        Route::POST('cost','TransactionController@cekOngkir');
        Route::POST('cart/{id}/update','TransactionCOntroller@updateItem');
        Route::delete('cart/{id}/delete','TransactionCOntroller@deleteItem');
        Route::get('carts','TransactionController@getCarts');
        Route::get('categories','CategoryController@getCategories');
        Route::get('sizes','SizeController@getSizes');
        Route::get('provinces','ProvinceController@getProvince');
    });
// });

Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('activate', 'Auth\ActivationController@activate')->name('auth.activate');
// Route::get('/', 'HomeController@index');

Route::get('/about',function () {
    return view('about');
});

Route::get('lookbook', function () {
    $lookbooks = \App\Lookbook::with('detailLookbook')->get();
    return view('lookbook')->with('lookbooks',$lookbooks);
});

Route::get('lookbook/index','LookbookController@index');
Route::get('lookbook/{id}/show', 'LookbookController@show');
Route::get('lookbook/{id}/edit', 'LookbookController@edit');
Route::get('lookbook/create','LookbookController@create')->name('lookbook.create');
Route::post('lookbook','LookbookController@store')->name('lookbook.store');
Route::post('lookbook/{id}','LookbookController@update')->name('lookbook.update');
Route::delete('lookbook/{id}','LookbookController@destroy');
Route::get('send','LookbookController@sendEmail');
Auth::routes(['verify' => true]);

// Route::get('/', 'HomeController@index')->name('home');

Route::POST('notification/handler','TransactionController@notificationHandler');

// Route::get('/', function(){
//     return view('welcome');
// });


